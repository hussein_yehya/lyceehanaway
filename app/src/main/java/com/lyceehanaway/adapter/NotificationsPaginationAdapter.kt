package com.lyceehanaway.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.annotation.NonNull
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.lyceehanaway.R
import com.lyceehanaway.model.Notification
import com.lyceehanaway.utils.FontsUtils
import kotlinx.android.synthetic.main.item_notification.view.*
import java.util.*


class NotificationsPaginationAdapter(
    val itemClicklistener: ItemClickListener
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder?>() {
    private var itemsList: MutableList<Notification>?
    private var isLoadingAdded = false

    fun setItemsList(itemsList: MutableList<Notification>?) {
        this.itemsList = itemsList
    }


    fun getItemsList(): MutableList<Notification>? {
        return this.itemsList
    }

    @NonNull
    override fun onCreateViewHolder(
        @NonNull parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        var viewHolder: RecyclerView.ViewHolder? = null
        val inflater = LayoutInflater.from(parent.context)
        when (viewType) {
            ITEM -> {
                val viewItem: View = inflater.inflate(R.layout.item_notification, parent, false)
                viewHolder = ItemsViewHolder(viewItem)
            }
            LOADING -> {
                val viewLoading: View = inflater.inflate(R.layout.item_progress, parent, false)
                viewHolder = LoadingViewHolder(viewLoading)
            }
        }
        return viewHolder!!
    }

    override fun onBindViewHolder(@NonNull holder: RecyclerView.ViewHolder, position: Int) {
        try {

            val service: Notification = itemsList!![position]

            if (service != null) {

                when (getItemViewType(position)) {
                    ITEM -> {
                        val itemViewHolder = holder as ItemsViewHolder

                        mClickListener = itemClicklistener

                        val item = itemsList!![position]

                        holder.textview_body.text = item.description
                        holder.textview_title.text = item.title
                        holder.textview_date.text = item.date


                        if (item.isRead.equals("0")){

                            holder.imageview.setColorFilter(ContextCompat.getColor(holder.itemView.context, R.color.purple_200), android.graphics.PorterDuff.Mode.MULTIPLY)
                        }else{
                            holder.imageview.setColorFilter(ContextCompat.getColor(holder.itemView.context, R.color.black2), android.graphics.PorterDuff.Mode.MULTIPLY)

                        }

                        holder.itemView.setOnClickListener(object : View.OnClickListener {
                            override fun onClick(v: View?) {
                                if (mClickListener != null)
                                    mClickListener?.onItemClick(item)
                            }
                        })


                    }
                    LOADING -> {
                        val loadingViewHolder = holder as LoadingViewHolder
                        loadingViewHolder.progressBar.visibility = View.VISIBLE
                    }
                }
            }


        } catch (e: Exception) {
        }
    }


    override fun getItemViewType(position: Int): Int {
        return if (
            itemsList!!.get(position).id.equals("")
            &&
            isLoadingAdded
        ) LOADING else ITEM
    }

    fun addLoadingFooter() {
        isLoadingAdded = true
        add(Notification())
    }

    fun removeLoadingFooter() {
        isLoadingAdded = false
        val position = itemsList!!.size - 1
        val result: Notification = getItem(position)
        if (result != null) {
            itemsList!!.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun add(item: Notification) {
        itemsList!!.add(item)
        notifyItemInserted(itemsList!!.size - 1)
    }

    fun addAll(moveResults: List<Notification>) {
        for (result in moveResults) {
            add(result)
        }
        notifyDataSetChanged()
    }

    fun getItem(position: Int): Notification {
        return itemsList!![position]
    }

    fun clear() {
        itemsList!!.clear()
        notifyDataSetChanged()
    }


    inner class ItemsViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val textview_title = view.textview_title
        val textview_body = view.textview_body
        val textview_date = view.textview_date
        val imageview = view.imageview

        init {
            FontsUtils.getLatoRegular(textview_title, itemView.context)
            FontsUtils.getLatoRegular(textview_body, itemView.context)
            FontsUtils.getLatoRegular(textview_date, itemView.context)
        }


    }

    inner class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val progressBar: ProgressBar

        init {
            progressBar = itemView.findViewById<View>(R.id.loadmore_progress) as ProgressBar
        }
    }

    companion object {
        private const val LOADING = 0
        private const val ITEM = 1
        var mClickListener: ItemClickListener? = null


    }

    init {
        itemsList = LinkedList<Notification>()
    }

    override fun getItemCount(): Int {
        var count = 0
        (itemsList != null)
        count = itemsList!!.size

        return count
    }

    interface ItemClickListener {
        fun onItemClick(item: Notification)

    }

    fun markAsRead(id:String){
        for (i in itemsList!!){
            if (i.id.toString().equals(id)){
                i.isRead = "1"
            }
        }
        notifyDataSetChanged()
    }

}