package com.lyceehanaway.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lyceehanaway.R
import com.lyceehanaway.model.Menu
import com.lyceehanaway.utils.FontsUtils
import kotlinx.android.synthetic.main.item_menu.view.*


class MenuListAdapter(val mWords: ArrayList<Menu>, val itemClicklistener: ItemClickListener) :
    RecyclerView.Adapter<MenuListAdapter.ViewHolder>() {
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        mClickListener = itemClicklistener
        val item = mWords[position]

        holder.textview.text = item.name

        holder.imageview.setImageResource(item.icon)

        holder.itemView.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                if (itemClicklistener != null)
                    itemClicklistener?.onItemClick(item)
            }
        })
    }

    companion object {
        var mClickListener: ItemClickListener? = null
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_menu, parent, false))
    }


    override fun getItemCount(): Int {
        return mWords.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to
        val textview = view.textview_name
        val imageview = view.imageview

        init {
            FontsUtils.getLatoSemibold(textview, itemView.context)
        }
    }

    interface ItemClickListener {
        fun onItemClick(item: Menu)
    }


}