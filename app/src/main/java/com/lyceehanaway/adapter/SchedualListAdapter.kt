package com.lyceehanaway.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lyceehanaway.R
import com.lyceehanaway.model.Schedual
import com.lyceehanaway.utils.FontsUtils
import kotlinx.android.synthetic.main.item_schedual.view.*


class SchedualListAdapter(
    val mWords: ArrayList<Schedual>,
    val itemClicklistener: ItemClickListener
) :
    RecyclerView.Adapter<SchedualListAdapter.ViewHolder>() {
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        mClickListener = itemClicklistener
        val item = mWords[position]

        if (item.subject_title.toString().equals("") || item.subject_title.toString().equals("null")){
            holder.textview_class.text = item.period_title
        }else{
            holder.textview_class.text = item.subject_title
        }

        holder.textview_instructor.text = item.teacher
        holder.textview_from.text = item.startTime
        holder.textview_to.text = item.endTime


        holder.itemView.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                if (itemClicklistener != null)
                    itemClicklistener?.onItemClick(item)
            }
        })
    }

    companion object {
        var mClickListener: ItemClickListener? = null
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_schedual, parent, false))
    }


    override fun getItemCount(): Int {
        return mWords.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to
        val textview_class = view.textview_class
        val textview_instructor = view.textview_instructor
        val textview_from = view.textview_from
        val textview_to = view.textview_to

        init {
            FontsUtils.getLatoBlack(textview_to, itemView.context)
            FontsUtils.getLatoBlack(textview_from, itemView.context)
            FontsUtils.getLatoBlack(textview_class, itemView.context)
            FontsUtils.getLatoRegular(textview_instructor, itemView.context)
        }
    }

    interface ItemClickListener {
        fun onItemClick(item: Schedual)
    }


}