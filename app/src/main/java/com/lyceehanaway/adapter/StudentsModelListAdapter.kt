package com.lyceehanaway.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.lyceehanaway.R
import com.lyceehanaway.model.Student
import com.lyceehanaway.utils.FontsUtils
import kotlinx.android.synthetic.main.item_student.view.*

class StudentsModelListAdapter(
    private val list: ArrayList<Student>,
    private val listener: ItemClickListener
) : RecyclerView.Adapter<StudentsModelListAdapter.StudentsModelViewHolder>(), Filterable {
    private var searchList: List<Student>? = null
    var selectedPos = -1

    inner class StudentsModelViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val imageview = view.imageview
        val textview_name = view.textview_name
        val textview_class = view.textview_class

        init {
            FontsUtils.getLatoRegular(textview_name, view.context)
            FontsUtils.getLatoRegular(textview_class, view.context)

        }

    }

    init {
        this.searchList = list
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StudentsModelViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_student, parent, false)
        return StudentsModelViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: StudentsModelViewHolder, position: Int) {
        val searchList = searchList!![position]
        holder.textview_name.text = searchList.fullname
        holder.textview_class.text = searchList.className


        try {
            Glide.with(holder.itemView.context).load(searchList.photo_url.toString())
                .error(R.drawable.dumy)
                .placeholder(R.drawable.dumy)
                .into(holder.imageview)
        } catch (e: Exception) {
        }


        holder.itemView.setOnClickListener {
            selectedPos = position
            notifyDataSetChanged()
            listener.onItemClicked(searchList)
        }


    }

    override fun getItemCount(): Int {
        return searchList!!.size
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): Filter.FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    searchList = list
                } else {
                    val filteredList = ArrayList<Student>()
                    for (row in list) {
                        if (row.fullname!!.toLowerCase()
                                .contains(charString.toLowerCase()) || row!!.fullname!!.contains(
                                charSequence
                            )
                        ) {
                            filteredList.add(row)
                        }
                    }
                    searchList = filteredList
                }
                val filterResults = Filter.FilterResults()
                filterResults.values = searchList
                return filterResults
            }

            override fun publishResults(
                charSequence: CharSequence,
                filterResults: Filter.FilterResults
            ) {
                searchList =
                    filterResults.values as ArrayList<Student>
                notifyDataSetChanged()
            }
        }
    }

    interface ItemClickListener {
        fun onItemClicked(region: Student)
    }


}