package com.lyceehanaway.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lyceehanaway.R
import com.lyceehanaway.model.Grades
import com.lyceehanaway.utils.FontsUtils
import kotlinx.android.synthetic.main.item_grades_element.view.*


class GradesElementsListAdapter(val mWords: ArrayList<Grades>, val itemClicklistener: ItemClickListener) :
    RecyclerView.Adapter<GradesElementsListAdapter.ViewHolder>() {
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        mClickListener = itemClicklistener
        val item = mWords[position]

       holder.textview_grade.text = item.grade
        holder.textview_title.text = item.name


        holder.itemView.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                if (itemClicklistener != null)
                    itemClicklistener?.onItemClick(item)
            }
        })
    }

    companion object {
        var mClickListener: ItemClickListener? = null
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_grades_element, parent, false))
    }


    override fun getItemCount(): Int {
        return mWords.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to
        val textview_grade = view.textview_grade
        val textview_title = view.textview_title


        init {
            FontsUtils.getLatoBold(textview_grade, itemView.context)
            FontsUtils.getLatoBold(textview_title, itemView.context)
        }
    }

    interface ItemClickListener {
        fun onItemClick(item: Grades)
    }


}