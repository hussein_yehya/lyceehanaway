package com.lyceehanaway.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.lyceehanaway.R
import com.lyceehanaway.model.Notification
import com.lyceehanaway.model.SchoolEvent
import com.lyceehanaway.utils.FontsUtils
import kotlinx.android.synthetic.main.item_notification.view.*


class NotificationsAdapter(val mWords: ArrayList<Notification>, val itemClicklistener: ItemClickListener) :
    RecyclerView.Adapter<NotificationsAdapter.ViewHolder>() {
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        mClickListener = itemClicklistener
        val item = mWords[position]

        holder.textview_body.text = item.description
        holder.textview_title.text = item.title
        holder.textview_date.text = item.date

        if (item.isRead.equals("0")){

            holder.imageview.setColorFilter(ContextCompat.getColor(holder.itemView.context, R.color.purple_200), android.graphics.PorterDuff.Mode.MULTIPLY)
        }else{
            holder.imageview.setColorFilter(ContextCompat.getColor(holder.itemView.context, R.color.black2), android.graphics.PorterDuff.Mode.MULTIPLY)

        }

        holder.itemView.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                if (itemClicklistener != null)
                    itemClicklistener?.onItemClick(item)
            }
        })
    }

    companion object {
        var mClickListener: ItemClickListener? = null
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_notification, parent, false))
    }


    override fun getItemCount(): Int {
        return mWords.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to
        val textview_title = view.textview_title
        val textview_body = view.textview_body
        val textview_date = view.textview_date
        val imageview = view.imageview

        init {
            FontsUtils.getLatoRegular(textview_title, itemView.context)
            FontsUtils.getLatoRegular(textview_body, itemView.context)
            FontsUtils.getLatoRegular(textview_date, itemView.context)
        }
    }

    interface ItemClickListener {
        fun onItemClick(item: Notification)
    }


}