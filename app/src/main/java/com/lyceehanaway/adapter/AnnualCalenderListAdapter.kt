package com.lyceehanaway.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lyceehanaway.R
import com.lyceehanaway.model.Calender
import com.lyceehanaway.utils.FontsUtils
import kotlinx.android.synthetic.main.item_annual_calender.view.*


class AnnualCalenderListAdapter(val mWords: ArrayList<Calender>, val itemClicklistener: ItemClickListener) :
    RecyclerView.Adapter<AnnualCalenderListAdapter.ViewHolder>() {
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        mClickListener = itemClicklistener
        val item = mWords[position]

        holder.textview_date.text = item.date
        holder.textview_event.text = item.title


        holder.itemView.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                if (itemClicklistener != null)
                    itemClicklistener?.onItemClick(item)
            }
        })
    }

    companion object {
        var mClickListener: ItemClickListener? = null
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_annual_calender, parent, false))
    }


    override fun getItemCount(): Int {
        return mWords.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to
        val textview_date = view.textview_date
        val textview_event = view.textview_event

        init {
            FontsUtils.getLatoRegular(textview_date, itemView.context)
            FontsUtils.getLatoRegular(textview_event, itemView.context)
        }
    }

    interface ItemClickListener {
        fun onItemClick(item: Calender)
    }


}