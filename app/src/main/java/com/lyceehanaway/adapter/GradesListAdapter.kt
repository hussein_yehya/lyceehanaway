package com.lyceehanaway.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lyceehanaway.R
import com.lyceehanaway.model.Grades
import com.lyceehanaway.model.GradesResp
import com.lyceehanaway.utils.FontsUtils
import kotlinx.android.synthetic.main.item_grades.view.*


class GradesListAdapter(val mWords: ArrayList<GradesResp>, val itemClicklistener: ItemClickListener) :
        RecyclerView.Adapter<GradesListAdapter.ViewHolder>() {
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        mClickListener = itemClicklistener
        val item = mWords[position]

        holder.textview_avarage.text = item.average
        holder.textview_title.text = item.title


        if (item.grades.size>0){

            val mLayoutManager =
                LinearLayoutManager(holder.itemView.context, LinearLayoutManager.VERTICAL, false)
            holder.recyclerview_grades_elemnts.layoutManager = mLayoutManager
            val adapter =
                GradesElementsListAdapter(item.grades, object : GradesElementsListAdapter.ItemClickListener {
                    override fun onItemClick(item: Grades) {


                    }

                }
                )


            holder.rel_top.setOnClickListener {
                if (item.isExpanded) {
                    holder.imageview_arrow_down.animate().rotation(0f).setDuration(500).start();
                    holder.recyclerview_grades_elemnts.visibility = View.GONE
                } else {
                    holder.imageview_arrow_down.animate().rotation(180f).setDuration(500).start();
                    holder.recyclerview_grades_elemnts.visibility = View.VISIBLE
                }
                item.isExpanded = !item.isExpanded
            }

            if (!item.isExpanded) {
                holder.imageview_arrow_down.rotation =0f
                holder.recyclerview_grades_elemnts.visibility = View.GONE
            } else {
                holder.imageview_arrow_down.rotation =180f
                holder.recyclerview_grades_elemnts.visibility = View.VISIBLE
            }

            holder.imageview_arrow_down.setOnClickListener {
                holder.rel_top.callOnClick()
            }
            holder.recyclerview_grades_elemnts.adapter = adapter


            holder.textview_title.setOnClickListener {
                holder.rel_top.callOnClick()
            }
            holder.textview_avarage.setOnClickListener {
                holder.rel_top.callOnClick()
            }

            holder.itemView.setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View?) {
                    holder.rel_top.callOnClick()

                }
            })

            holder.imageview_arrow_down.visibility = View.VISIBLE
        }else{
            holder.imageview_arrow_down.visibility = View.GONE
            holder.itemView.setOnClickListener(null)
            holder.textview_avarage.setOnClickListener(null)
            holder.textview_title.setOnClickListener(null)
            holder.rel_top.setOnClickListener(null)
            holder.recyclerview_grades_elemnts.visibility = View.GONE

        }

    }

    companion object {
        var mClickListener: ItemClickListener? = null
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_grades, parent, false))
    }


    override fun getItemCount(): Int {
        return mWords.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to
        val textview_avarage = view.textview_avarage
        val textview_title = view.textview_title
        val imageview_arrow_down = view.imageview_arrow_down
        val recyclerview_grades_elemnts = view.recyclerview_grades_elemnts
        val rel_top = view.rel_top

        init {
            FontsUtils.getLatoBold(textview_avarage, itemView.context)
            FontsUtils.getLatoBold(textview_title, itemView.context)
        }
    }

    interface ItemClickListener {
        fun onItemClick(item: GradesResp)
    }


}