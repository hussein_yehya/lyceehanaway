package com.lyceehanaway.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lyceehanaway.R
import com.lyceehanaway.model.SchoolEvent
import com.lyceehanaway.model.SchoolEventResp
import com.lyceehanaway.utils.FontsUtils
import kotlinx.android.synthetic.main.item_school_events_list.view.*


class SchoolEventsListAdapter(
    val mWords: ArrayList<SchoolEventResp>,
    val itemClicklistener: ItemClickListener
) :
    RecyclerView.Adapter<SchoolEventsListAdapter.ViewHolder>() {
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        mClickListener = itemClicklistener
        val item = mWords[position]

        holder.textview_date.text = item.date


        val mLayoutManager =
            LinearLayoutManager(holder.itemView.context, LinearLayoutManager.VERTICAL, false)


        holder.receyclerview_events.layoutManager = mLayoutManager

        var adapter =
            SchoolEventsAdapter(item.events, object : SchoolEventsAdapter.ItemClickListener {
                override fun onItemClick(item: SchoolEvent) {
                    if (itemClicklistener != null)
                        itemClicklistener?.onItemClick(item)
                }
            }
            )

        holder.receyclerview_events.adapter = adapter


    }

    companion object {
        var mClickListener: ItemClickListener? = null
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_school_events_list, parent, false))
    }


    override fun getItemCount(): Int {
        return mWords.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to
        val textview_date = view.textview_date
        val receyclerview_events = view.receyclerview_events

        init {
            FontsUtils.getLatoBold(textview_date, itemView.context)
        }
    }

    interface ItemClickListener {
        fun onItemClick(item: SchoolEvent)
    }


}