package com.lyceehanaway.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.lyceehanaway.R;
import com.lyceehanaway.model.Slider;

import java.util.ArrayList;


public class SliderPagerAdapter extends PagerAdapter {
    Activity activity;
    ArrayList<Slider> image_arraylist;
    private LayoutInflater layoutInflater;

    public SliderPagerAdapter(Activity activity, ArrayList<Slider> image_arraylist) {
        this.activity = activity;
        this.image_arraylist = image_arraylist;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = layoutInflater.inflate(R.layout.fragment_slide, container, false);
        ImageView im_slider = view.findViewById(R.id.imageview);

        try {
            Glide.with(activity.getApplicationContext()).load(image_arraylist.get(position).getSlider_url())
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                    .into(im_slider);
        } catch (Exception e) {
        }


        im_slider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



            }
        });

        container.addView(view);

        return view;
    }

    @Override
    public int getCount() {
        return image_arraylist.size();
    }


    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }
}
