package com.lyceehanaway.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lyceehanaway.R
import com.lyceehanaway.model.Agenda
import com.lyceehanaway.utils.FontsUtils
import kotlinx.android.synthetic.main.item_agenda.view.*


class AgendaListAdapter(val mWords: ArrayList<Agenda>, val itemClicklistener: ItemClickListener) :
    RecyclerView.Adapter<AgendaListAdapter.ViewHolder>() {
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        mClickListener = itemClicklistener
        val item = mWords[position]

        holder.textview_date.text = item.date
        holder.textview_type_value.text = item.category
        holder.textview_class.text = item.subject
        holder.textview_details_val.text = item.body


        holder.itemView.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                if (itemClicklistener != null)
                    itemClicklistener?.onItemClick(item)
            }
        })
    }

    companion object {
        var mClickListener: ItemClickListener? = null
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_agenda, parent, false))
    }


    override fun getItemCount(): Int {
        return mWords.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to
        val textview_date = view.textview_date
        val textview_class = view.textview_class
        val textview_type = view.textview_type
        val textview_type_value = view.textview_type_value
        val textview_details = view.textview_details
        val textview_details_val = view.textview_details_val

        init {
            FontsUtils.getLatoRegular(textview_date, itemView.context)
            FontsUtils.getLatoRegular(textview_details_val, itemView.context)
            FontsUtils.getLatoRegular(textview_type_value, itemView.context)
            FontsUtils.getLatoBold(textview_details, itemView.context)
            FontsUtils.getLatoBold(textview_type, itemView.context)
            FontsUtils.getLatoBold(textview_class, itemView.context)
        }
    }

    interface ItemClickListener {
        fun onItemClick(item: Agenda)
    }


}