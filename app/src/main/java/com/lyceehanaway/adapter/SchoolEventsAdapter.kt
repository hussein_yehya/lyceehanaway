package com.lyceehanaway.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lyceehanaway.R
import com.lyceehanaway.model.SchoolEvent
import com.lyceehanaway.utils.FontsUtils
import kotlinx.android.synthetic.main.item_school_event.view.*


class SchoolEventsAdapter(val mWords: ArrayList<SchoolEvent>, val itemClicklistener: ItemClickListener) :
    RecyclerView.Adapter<SchoolEventsAdapter.ViewHolder>() {
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        mClickListener = itemClicklistener
        val item = mWords[position]

        holder.textview_body.text = item.description
        holder.textview_title.text = item.title


        holder.itemView.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                if (itemClicklistener != null)
                    itemClicklistener?.onItemClick(item)
            }
        })
    }

    companion object {
        var mClickListener: ItemClickListener? = null
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_school_event, parent, false))
    }


    override fun getItemCount(): Int {
        return mWords.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to
        val textview_title = view.textview_title
        val textview_body = view.textview_body
        val imageview = view.imageview

        init {
            FontsUtils.getLatoRegular(textview_title, itemView.context)
            FontsUtils.getLatoRegular(textview_body, itemView.context)
        }
    }

    interface ItemClickListener {
        fun onItemClick(item: SchoolEvent)
    }


}