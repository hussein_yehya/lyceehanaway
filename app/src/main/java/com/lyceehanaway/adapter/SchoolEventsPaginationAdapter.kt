package com.lyceehanaway.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.annotation.NonNull
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lyceehanaway.R
import com.lyceehanaway.model.SchoolEvent
import com.lyceehanaway.model.SchoolEventResp
import com.lyceehanaway.utils.FontsUtils
import kotlinx.android.synthetic.main.item_school_events_list.view.*
import java.util.*


class SchoolEventsPaginationAdapter(
    val itemClicklistener: ItemClickListener
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder?>() {
    private var itemsList: MutableList<SchoolEventResp>?
    private var isLoadingAdded = false

    fun setItemsList(itemsList: MutableList<SchoolEventResp>?) {
        this.itemsList = itemsList
    }


    fun getItemsList(): MutableList<SchoolEventResp>? {
        return this.itemsList
    }

    @NonNull
    override fun onCreateViewHolder(
        @NonNull parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        var viewHolder: RecyclerView.ViewHolder? = null
        val inflater = LayoutInflater.from(parent.context)
        when (viewType) {
            ITEM -> {
                val viewItem: View =
                    inflater.inflate(R.layout.item_school_events_list, parent, false)
                viewHolder = ItemsViewHolder(viewItem)
            }
            LOADING -> {
                val viewLoading: View = inflater.inflate(R.layout.item_progress, parent, false)
                viewHolder = LoadingViewHolder(viewLoading)
            }
        }
        return viewHolder!!
    }

    override fun onBindViewHolder(@NonNull holder: RecyclerView.ViewHolder, position: Int) {
        try {

            val service: SchoolEventResp = itemsList!![position]

            if (service != null) {

                when (getItemViewType(position)) {
                    ITEM -> {
                        val itemViewHolder = holder as ItemsViewHolder

                        mClickListener = itemClicklistener

                        val item = itemsList!![position]


                        holder.textview_date.text = item.date


                        val mLayoutManager =
                            LinearLayoutManager(
                                holder.itemView.context,
                                LinearLayoutManager.VERTICAL,
                                false
                            )


                        holder.receyclerview_events.layoutManager = mLayoutManager

                        var adapter =
                            SchoolEventsAdapter(
                                item.events,
                                object : SchoolEventsAdapter.ItemClickListener {
                                    override fun onItemClick(item: SchoolEvent) {
                                        if (itemClicklistener != null)
                                            itemClicklistener?.onItemClick(item)
                                    }
                                }
                            )

                        holder.receyclerview_events.adapter = adapter


//                        holder.itemView.setOnClickListener(object : View.OnClickListener {
//                            override fun onClick(v: View?) {
//                                if (mClickListener != null)
//                                    mClickListener?.onItemClick(item)
//                            }
//                        })


                    }
                    LOADING -> {
                        val loadingViewHolder = holder as LoadingViewHolder
                        loadingViewHolder.progressBar.visibility = View.VISIBLE
                    }
                }
            }


        } catch (e: Exception) {
        }
    }


    override fun getItemViewType(position: Int): Int {
        return if (
            itemsList!!.get(position).date.equals("")
            &&
            isLoadingAdded
        ) LOADING else ITEM
    }

    fun addLoadingFooter() {
        isLoadingAdded = true
        add(SchoolEventResp())
    }

    fun removeLoadingFooter() {
        isLoadingAdded = false
        val position = itemsList!!.size - 1
        val result: SchoolEventResp = getItem(position)
        if (result != null) {
            itemsList!!.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun add(item: SchoolEventResp) {
        itemsList!!.add(item)
        notifyItemInserted(itemsList!!.size - 1)
    }

    fun addAll(moveResults: List<SchoolEventResp>) {
        for (result in moveResults) {
            add(result)
        }
        notifyDataSetChanged()
    }

    fun getItem(position: Int): SchoolEventResp {
        return itemsList!![position]
    }

    fun clear() {
        itemsList!!.clear()
        notifyDataSetChanged()
    }


    inner class ItemsViewHolder(view: View) : RecyclerView.ViewHolder(view) {


        // Holds the TextView that will add each animal to
        val textview_date = view.textview_date
        val receyclerview_events = view.receyclerview_events

        init {
            FontsUtils.getLatoBold(textview_date, itemView.context)
        }

    }

    inner class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val progressBar: ProgressBar

        init {
            progressBar = itemView.findViewById<View>(R.id.loadmore_progress) as ProgressBar
        }
    }

    companion object {
        private const val LOADING = 0
        private const val ITEM = 1
        var mClickListener: ItemClickListener? = null


    }

    init {
        itemsList = LinkedList<SchoolEventResp>()
    }

    override fun getItemCount(): Int {
        var count = 0
        (itemsList != null)
        count = itemsList!!.size

        return count
    }

    interface ItemClickListener {
        fun onItemClick(item: SchoolEvent)

    }


}