package com.lyceehanaway.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lyceehanaway.R
import com.lyceehanaway.model.Absence
import com.lyceehanaway.model.Card
import com.lyceehanaway.utils.FontsUtils
import kotlinx.android.synthetic.main.item_card.view.*


class CardsListAdapter(val mWords: ArrayList<Card>, val itemClicklistener: ItemClickListener) :
    RecyclerView.Adapter<CardsListAdapter.ViewHolder>() {
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        mClickListener = itemClicklistener
        val item = mWords[position]

        holder.textview_name.text = item.cardTitle


        holder.itemView.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                if (itemClicklistener != null)
                    itemClicklistener?.onItemClick(item)
            }
        })
    }

    companion object {
        var mClickListener: ItemClickListener? = null
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_card, parent, false))
    }


    override fun getItemCount(): Int {
        return mWords.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to
        val textview_name = view.textview_name

        init {
            FontsUtils.getLatoBold(textview_name, itemView.context)
         }
    }

    interface ItemClickListener {
        fun onItemClick(item: Card)
    }


}