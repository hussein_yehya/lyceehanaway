package com.lyceehanaway.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.lyceehanaway.R
import com.lyceehanaway.model.Absence
import com.lyceehanaway.utils.FontsUtils
import kotlinx.android.synthetic.main.item_image.view.*


class ImagesListAdapter(val mWords: ArrayList<String>, val itemClicklistener: ItemClickListener) :
    RecyclerView.Adapter<ImagesListAdapter.ViewHolder>() {
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        mClickListener = itemClicklistener
        val item = mWords[position]

        try {
            Glide.with(holder.itemView.context).load(item.toString()).into(holder.imageview)
        }catch (e:Exception){}

        holder.itemView.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                if (itemClicklistener != null)
                    itemClicklistener?.onItemClick(item)
            }
        })
    }

    companion object {
        var mClickListener: ItemClickListener? = null
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_image, parent, false))
    }


    override fun getItemCount(): Int {
        return mWords.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to
        val imageview = view.imageview


    }

    interface ItemClickListener {
        fun onItemClick(item: String)
    }


}