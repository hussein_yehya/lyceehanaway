package com.lyceehanaway.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lyceehanaway.R
import com.lyceehanaway.model.Payment
import com.lyceehanaway.model.SchoolEvent
import com.lyceehanaway.utils.FontsUtils
import kotlinx.android.synthetic.main.item_payment.view.*
import kotlinx.android.synthetic.main.item_school_event.view.*


class PaymentsAdapter(val mWords: ArrayList<Payment>, val itemClicklistener: ItemClickListener) :
    RecyclerView.Adapter<PaymentsAdapter.ViewHolder>() {
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        mClickListener = itemClicklistener
        val item = mWords[position]

        holder.textview_amount_value.text = item.amount
        holder.textview_date_val.text = item.dueDate
        holder.textview_payment_title_val.text = item.paymentTitle


        holder.itemView.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                if (itemClicklistener != null)
                    itemClicklistener?.onItemClick(item)
            }
        })
    }

    companion object {
        var mClickListener: ItemClickListener? = null
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_payment, parent, false))
    }


    override fun getItemCount(): Int {
        return mWords.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to
        val textview_amount = view.textview_amount
        val textview_amount_value = view.textview_amount_value
        val textview_date = view.textview_date
        val textview_date_val = view.textview_date_val
        val textview_payment_title = view.textview_payment_title
        val textview_payment_title_val = view.textview_payment_title_val


        init {
            FontsUtils.getLatoBold(textview_amount, itemView.context)
            FontsUtils.getLatoBold(textview_date, itemView.context)
            FontsUtils.getLatoBold(textview_payment_title, itemView.context)

            FontsUtils.getLatoRegular(textview_amount_value, itemView.context)
            FontsUtils.getLatoRegular(textview_date_val, itemView.context)
            FontsUtils.getLatoRegular(textview_payment_title_val, itemView.context)
        }
    }

    interface ItemClickListener {
        fun onItemClick(item: Payment)
    }


}