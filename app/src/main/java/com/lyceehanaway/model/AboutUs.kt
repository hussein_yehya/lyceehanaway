package com.lyceehanaway.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class AboutUs() : Parcelable {

    @SerializedName("title")
    var title: String? = ""
    @SerializedName("description")
    var description: String? = ""
    @SerializedName("image")
    var image: String? = ""
    @SerializedName("phone")
    var phone: String? = ""
    @SerializedName("email")
    var email: String? = ""
    @SerializedName("address")
    var address: String? = ""

    constructor(title: String,description: String,image: String,phone: String,email: String,address: String) : this() {
        this.title=title
        this.description=description
        this.image=image
        this.phone=phone
        this.email=email
        this.address=address
    }

    constructor(parcel: Parcel) : this() {
        title = parcel.readString()
        description = parcel.readString()
        image = parcel.readString()
        phone = parcel.readString()
        email = parcel.readString()
        address = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(title)
        parcel.writeString(description)
        parcel.writeString(image)
        parcel.writeString(phone)
        parcel.writeString(email)
        parcel.writeString(address)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AboutUs> {
        override fun createFromParcel(parcel: Parcel): AboutUs {
            return AboutUs(parcel)
        }

        override fun newArray(size: Int): Array<AboutUs?> {
            return arrayOfNulls(size)
        }
    }


}

