package com.lyceehanaway.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class Notification() : Parcelable {

    @SerializedName("id")
    var id: String? = ""

    @SerializedName("title")
    var title: String? = ""

    @SerializedName("intro")
    var intro: String? = ""

    @SerializedName("description")
    var description: String? = ""

    @SerializedName("photo_url")
    var photo_url: String? = ""

    @SerializedName("date")
    var date: String? = ""

    @SerializedName("fromDate")
    var fromDate: String? = ""

    @SerializedName("toDate")
    var toDate: String? = ""

    @SerializedName("isRead")
    var isRead: String? = ""

    @SerializedName("type")
    var type: String? = ""

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        title = parcel.readString()
        intro = parcel.readString()
        description = parcel.readString()
        photo_url = parcel.readString()
        date = parcel.readString()
        fromDate = parcel.readString()
        toDate = parcel.readString()
        isRead = parcel.readString()
        type = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(title)
        parcel.writeString(intro)
        parcel.writeString(description)
        parcel.writeString(photo_url)
        parcel.writeString(date)
        parcel.writeString(fromDate)
        parcel.writeString(toDate)
        parcel.writeString(isRead)
        parcel.writeString(type)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Notification> {
        override fun createFromParcel(parcel: Parcel): Notification {
            return Notification(parcel)
        }

        override fun newArray(size: Int): Array<Notification?> {
            return arrayOfNulls(size)
        }
    }


}

