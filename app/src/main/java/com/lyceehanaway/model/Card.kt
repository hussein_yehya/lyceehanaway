package com.lyceehanaway.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class Card() : Parcelable {

    @SerializedName("idclassCard")
    var idclassCard: String? = ""

    @SerializedName("cardTitle")
    var cardTitle: String? = ""

    @SerializedName("cardElements")
    var cardElements = ArrayList<CardElement>()

    constructor(parcel: Parcel) : this() {
        idclassCard = parcel.readString()
        cardTitle = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(idclassCard)
        parcel.writeString(cardTitle)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Card> {
        override fun createFromParcel(parcel: Parcel): Card {
            return Card(parcel)
        }

        override fun newArray(size: Int): Array<Card?> {
            return arrayOfNulls(size)
        }
    }


}

