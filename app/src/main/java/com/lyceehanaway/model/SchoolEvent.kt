package com.lyceehanaway.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class SchoolEvent() : Parcelable {

    @SerializedName("id")
    var id: String? = ""

    @SerializedName("title")
    var title: String? = ""

    @SerializedName("description")
    var description: String? = ""

    @SerializedName("photo_url")
    var photo_url: String? = ""

    @SerializedName("date")
    var date: String? = ""

    @SerializedName("fromDate")
    var fromDate: String? = ""

    @SerializedName("toDate")
    var toDate: String? = ""

    @SerializedName("fromTime")
    var fromTime: String? = ""

    @SerializedName("toTime")
    var toTime: String? = ""

    @SerializedName("images")
    var images = ArrayList<String>()

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        title = parcel.readString()
        description = parcel.readString()
        photo_url = parcel.readString()
        date = parcel.readString()
        fromDate = parcel.readString()
        toDate = parcel.readString()
        fromTime = parcel.readString()
        toTime = parcel.readString()
        images = parcel.createStringArrayList()!!

    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(title)
        parcel.writeString(description)
        parcel.writeString(photo_url)
        parcel.writeString(date)
        parcel.writeString(fromDate)
        parcel.writeString(toDate)
        parcel.writeString(fromTime)
        parcel.writeString(toTime)
        parcel.writeStringList(images)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SchoolEvent> {
        override fun createFromParcel(parcel: Parcel): SchoolEvent {
            return SchoolEvent(parcel)
        }

        override fun newArray(size: Int): Array<SchoolEvent?> {
            return arrayOfNulls(size)
        }
    }


}

