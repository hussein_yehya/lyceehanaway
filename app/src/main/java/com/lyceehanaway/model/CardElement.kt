package com.lyceehanaway.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class CardElement() : Parcelable {

    @SerializedName("cardElements_idcardElements")
    var cardElements_idcardElements: String? = ""

    @SerializedName("display")
    var display: String? = ""

    constructor(parcel: Parcel) : this() {
        cardElements_idcardElements = parcel.readString()
        display = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(cardElements_idcardElements)
        parcel.writeString(display)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CardElement> {
        override fun createFromParcel(parcel: Parcel): CardElement {
            return CardElement(parcel)
        }

        override fun newArray(size: Int): Array<CardElement?> {
            return arrayOfNulls(size)
        }
    }


}

