package com.lyceehanaway.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class SchedualListResp() : Parcelable {

    @SerializedName("1")
    var mon = ArrayList<Schedual>()

    @SerializedName("2")
    var tue = ArrayList<Schedual>()

    @SerializedName("3")
    var wed = ArrayList<Schedual>()

    @SerializedName("4")
    var thu = ArrayList<Schedual>()

    @SerializedName("5")
    var fri = ArrayList<Schedual>()

    constructor(parcel: Parcel) : this() {

    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SchedualListResp> {
        override fun createFromParcel(parcel: Parcel): SchedualListResp {
            return SchedualListResp(parcel)
        }

        override fun newArray(size: Int): Array<SchedualListResp?> {
            return arrayOfNulls(size)
        }
    }


}

