package com.lyceehanaway.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class AgendaResp() : Parcelable {

    @SerializedName("agenda")
    var agenda = ArrayList<Agenda>()

    @SerializedName("agendaDates")
    var agendaDates = ArrayList<String>()

    constructor(parcel: Parcel) : this() {

    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AgendaResp> {
        override fun createFromParcel(parcel: Parcel): AgendaResp {
            return AgendaResp(parcel)
        }

        override fun newArray(size: Int): Array<AgendaResp?> {
            return arrayOfNulls(size)
        }
    }


}

