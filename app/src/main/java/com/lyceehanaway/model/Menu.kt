package com.lyceehanaway.model

import android.os.Parcel
import android.os.Parcelable

class Menu() : Parcelable {

    var name: String = ""
    var icon: Int = 0
    var id: Int = 0


    constructor(id: Int, icon: Int, name: String) : this() {
        this.icon = icon
        this.name = name
        this.id = id
    }


    constructor(parcel: Parcel) : this() {
        name = parcel.readString().toString()
        icon = parcel.readInt()
        id = parcel.readInt()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeInt(icon)
        parcel.writeInt(id)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Menu> {
        override fun createFromParcel(parcel: Parcel): Menu {
            return Menu(parcel)
        }

        override fun newArray(size: Int): Array<Menu?> {
            return arrayOfNulls(size)
        }
    }


}
