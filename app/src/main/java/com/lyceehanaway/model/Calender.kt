package com.lyceehanaway.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class Calender() : Parcelable {

    @SerializedName("id")
    var id: String? = ""

    @SerializedName("title")
    var title: String? = ""

    @SerializedName("description")
    var description: String? = ""

    @SerializedName("date")
    var date: String? = ""

    @SerializedName("fromDate")
    var fromDate: String? = ""

    @SerializedName("toDate")
    var toDate: String? = ""

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        title = parcel.readString()
        description = parcel.readString()
        date = parcel.readString()
        fromDate = parcel.readString()
        toDate = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(title)
        parcel.writeString(description)
        parcel.writeString(date)
        parcel.writeString(fromDate)
        parcel.writeString(toDate)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Calender> {
        override fun createFromParcel(parcel: Parcel): Calender {
            return Calender(parcel)
        }

        override fun newArray(size: Int): Array<Calender?> {
            return arrayOfNulls(size)
        }
    }


}

