package com.lyceehanaway.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class NotificationsResp() : Parcelable {

    @SerializedName("count")
    var count: String? = ""

    @SerializedName("notifications")
    var notifications = ArrayList<Notification>()

    constructor(parcel: Parcel) : this() {
        count = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(count)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<NotificationsResp> {
        override fun createFromParcel(parcel: Parcel): NotificationsResp {
            return NotificationsResp(parcel)
        }

        override fun newArray(size: Int): Array<NotificationsResp?> {
            return arrayOfNulls(size)
        }
    }


}

