package com.lyceehanaway.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class Absence() : Parcelable {

    @SerializedName("date")
    var date: String? = ""

    @SerializedName("reason")
    var reason: String? = ""

    constructor(parcel: Parcel) : this() {
        date = parcel.readString()
        reason = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(date)
        parcel.writeString(reason)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Absence> {
        override fun createFromParcel(parcel: Parcel): Absence {
            return Absence(parcel)
        }

        override fun newArray(size: Int): Array<Absence?> {
            return arrayOfNulls(size)
        }
    }


}

