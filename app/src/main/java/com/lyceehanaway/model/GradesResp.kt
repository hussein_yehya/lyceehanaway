package com.lyceehanaway.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class GradesResp() : Parcelable {

    @SerializedName("title")
    var title: String? = ""

    @SerializedName("average")
    var average: String? = ""

    @SerializedName("grades")
    var grades = ArrayList<Grades>()

    var isExpanded: Boolean = false

    constructor(parcel: Parcel) : this() {
        title = parcel.readString()
        average = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(title)
        parcel.writeString(average)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<GradesResp> {
        override fun createFromParcel(parcel: Parcel): GradesResp {
            return GradesResp(parcel)
        }

        override fun newArray(size: Int): Array<GradesResp?> {
            return arrayOfNulls(size)
        }
    }

}

