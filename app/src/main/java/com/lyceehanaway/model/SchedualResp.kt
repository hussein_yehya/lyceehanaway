package com.lyceehanaway.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class SchedualResp() : Parcelable {

    @SerializedName("photo")
    var photo: String = ""

    @SerializedName("schedule")
    var schedule = SchedualListResp()

    constructor(parcel: Parcel) : this() {
        photo = parcel.readString().toString()
        schedule = parcel.readParcelable(SchedualListResp::class.java.classLoader)!!
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(photo)
        parcel.writeParcelable(schedule, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SchedualResp> {
        override fun createFromParcel(parcel: Parcel): SchedualResp {
            return SchedualResp(parcel)
        }

        override fun newArray(size: Int): Array<SchedualResp?> {
            return arrayOfNulls(size)
        }
    }


}

