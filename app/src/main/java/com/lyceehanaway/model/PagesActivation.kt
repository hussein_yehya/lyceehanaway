package com.lyceehanaway.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class PagesActivation() : Parcelable {

    @SerializedName("title")
    var title: String? = ""

    @SerializedName("code")
    var code: String? = ""

    constructor(parcel: Parcel) : this() {
        title = parcel.readString()
        code = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(title)
        parcel.writeString(code)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PagesActivation> {
        override fun createFromParcel(parcel: Parcel): PagesActivation {
            return PagesActivation(parcel)
        }

        override fun newArray(size: Int): Array<PagesActivation?> {
            return arrayOfNulls(size)
        }
    }


}

