package com.lyceehanaway.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class SliderResp() : Parcelable {

    @SerializedName("slider")
    var slider = ArrayList<Slider>()

    @SerializedName("pages")
    var pages =  ArrayList<PagesActivation>()

    constructor(parcel: Parcel) : this() {
        pages = parcel.readParcelable(PagesActivationResp::class.java.classLoader)!!
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SliderResp> {
        override fun createFromParcel(parcel: Parcel): SliderResp {
            return SliderResp(parcel)
        }

        override fun newArray(size: Int): Array<SliderResp?> {
            return arrayOfNulls(size)
        }
    }


}

