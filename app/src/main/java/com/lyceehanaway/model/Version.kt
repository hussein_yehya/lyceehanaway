package com.lyceehanaway.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class Version() : Parcelable {


    @SerializedName("version")
    var version: String? = ""

    @SerializedName("forceUpdate")
    var forceUpdate: String? = ""


    constructor(parcel: Parcel) : this() {
        version = parcel.readString()
        forceUpdate = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(version)
        parcel.writeString(forceUpdate)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Version> {
        override fun createFromParcel(parcel: Parcel): Version {
            return Version(parcel)
        }

        override fun newArray(size: Int): Array<Version?> {
            return arrayOfNulls(size)
        }
    }


}
