package com.lyceehanaway.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class Student() : Parcelable {

    @SerializedName("id")
    var id: String? = ""

    @SerializedName("fullname")
    var fullname: String? = ""

    @SerializedName("className")
    var className: String? = ""

    @SerializedName("photo_url")
    var photo_url: String? = ""

    @SerializedName("classSectionID")
    var classSectionID: String? = ""

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        fullname = parcel.readString()
        className = parcel.readString()
        photo_url = parcel.readString()
        classSectionID = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(fullname)
        parcel.writeString(className)
        parcel.writeString(photo_url)
        parcel.writeString(classSectionID)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Student> {
        override fun createFromParcel(parcel: Parcel): Student {
            return Student(parcel)
        }

        override fun newArray(size: Int): Array<Student?> {
            return arrayOfNulls(size)
        }
    }


}

