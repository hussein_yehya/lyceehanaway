package com.lyceehanaway.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class PaymentResp() : Parcelable {


    @SerializedName("type")
    var type: String? = ""

    @SerializedName("payments")
    var payments = ArrayList<Payment>()

    constructor(parcel: Parcel) : this() {
        type = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(type)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PaymentResp> {
        override fun createFromParcel(parcel: Parcel): PaymentResp {
            return PaymentResp(parcel)
        }

        override fun newArray(size: Int): Array<PaymentResp?> {
            return arrayOfNulls(size)
        }
    }


}

