package com.lyceehanaway.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class Payment() : Parcelable {

    @SerializedName("paymentTitle")
    var paymentTitle: String? = ""

    @SerializedName("amount")
    var amount: String? = ""

    @SerializedName("dueDate")
    var dueDate: String? = ""

    constructor(parcel: Parcel) : this() {
        paymentTitle = parcel.readString()
        amount = parcel.readString()
        dueDate = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(paymentTitle)
        parcel.writeString(amount)
        parcel.writeString(dueDate)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Payment> {
        override fun createFromParcel(parcel: Parcel): Payment {
            return Payment(parcel)
        }

        override fun newArray(size: Int): Array<Payment?> {
            return arrayOfNulls(size)
        }
    }


}

