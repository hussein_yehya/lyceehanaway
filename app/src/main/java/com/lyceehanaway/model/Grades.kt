package com.lyceehanaway.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class Grades() : Parcelable {

    @SerializedName("name")
    var name: String? = ""

    @SerializedName("grade")
    var grade: String? = ""

    constructor(parcel: Parcel) : this() {
        name = parcel.readString()
        grade = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(grade)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Grades> {
        override fun createFromParcel(parcel: Parcel): Grades {
            return Grades(parcel)
        }

        override fun newArray(size: Int): Array<Grades?> {
            return arrayOfNulls(size)
        }
    }


}

