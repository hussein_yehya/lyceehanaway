package com.lyceehanaway.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class Slider() : Parcelable {


    @SerializedName("slider_url")
    var slider_url: String? = ""


    constructor(parcel: Parcel) : this() {
        slider_url = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(slider_url)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Slider> {
        override fun createFromParcel(parcel: Parcel): Slider {
            return Slider(parcel)
        }

        override fun newArray(size: Int): Array<Slider?> {
            return arrayOfNulls(size)
        }
    }


}

