package com.lyceehanaway.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class CalenderResp() : Parcelable {

    @SerializedName("calendar")
    var calendar = ArrayList<Calender>()

    @SerializedName("calendarDates")
    var calendarDates = ArrayList<String>()

    constructor(parcel: Parcel) : this() {

    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CalenderResp> {
        override fun createFromParcel(parcel: Parcel): CalenderResp {
            return CalenderResp(parcel)
        }

        override fun newArray(size: Int): Array<CalenderResp?> {
            return arrayOfNulls(size)
        }
    }


}

