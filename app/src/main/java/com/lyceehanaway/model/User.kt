package com.lyceehanaway.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class User() : Parcelable {

    @SerializedName("id")
    var id: String? = ""

    @SerializedName("fullname")
    var fullname: String? = ""

    @SerializedName("photo_url")
    var photo_url: String? = ""

    @SerializedName("islogin")
    var islogin: String? = ""

    @SerializedName("type")
    var type: String? = ""

    @SerializedName("version")
    var version: String? = ""

    @SerializedName("is_instructor")
    var is_instructor: String? = ""

    @SerializedName("token")
    var token: String? = ""

    @SerializedName("username")
    var username: String? = ""

    @SerializedName("password")
    var password: String? = ""

    @SerializedName("pages")
    var pages=ArrayList<PagesActivation>()

    constructor(username: String,password: String,fullname: String) : this() {
        this.username=username
        this.password=password
        this.fullname=fullname
    }


    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        fullname = parcel.readString()
        photo_url = parcel.readString()
        islogin = parcel.readString()
        type = parcel.readString()
        version = parcel.readString()
        is_instructor = parcel.readString()
        token = parcel.readString()
        username = parcel.readString()
        token = parcel.readString()
        password = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(fullname)
        parcel.writeString(photo_url)
        parcel.writeString(islogin)
        parcel.writeString(type)
        parcel.writeString(version)
        parcel.writeString(is_instructor)
        parcel.writeString(token)
        parcel.writeString(username)
        parcel.writeString(password)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<User> {
        override fun createFromParcel(parcel: Parcel): User {
            return User(parcel)
        }

        override fun newArray(size: Int): Array<User?> {
            return arrayOfNulls(size)
        }
    }


}

