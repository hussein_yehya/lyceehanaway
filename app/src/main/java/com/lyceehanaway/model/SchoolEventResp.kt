package com.lyceehanaway.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class SchoolEventResp() : Parcelable {


    @SerializedName("date")
    var date: String? = ""

    @SerializedName("events")
    var events = ArrayList<SchoolEvent>()

    constructor(parcel: Parcel) : this() {
        date = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(date)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SchoolEventResp> {
        override fun createFromParcel(parcel: Parcel): SchoolEventResp {
            return SchoolEventResp(parcel)
        }

        override fun newArray(size: Int): Array<SchoolEventResp?> {
            return arrayOfNulls(size)
        }
    }


}

