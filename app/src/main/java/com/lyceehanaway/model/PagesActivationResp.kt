package com.lyceehanaway.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class PagesActivationResp() : Parcelable {

    @SerializedName("m_parents_students")
    var m_parents_students = PagesActivation()

    @SerializedName("m_parents_events")
    var m_parents_events = PagesActivation()

    @SerializedName("m_parents_payments")
    var m_parents_payments = PagesActivation()

    @SerializedName("m_parents_notifications")
    var m_parents_notifications = PagesActivation()

    @SerializedName("m_parents_monthly_calendar")
    var m_parents_monthly_calendar = PagesActivation()

    @SerializedName("m_parents_call_us")
    var m_parents_call_us = PagesActivation()

    constructor(parcel: Parcel) : this() {
        m_parents_students = parcel.readParcelable(PagesActivation::class.java.classLoader)!!
        m_parents_events = parcel.readParcelable(PagesActivation::class.java.classLoader)!!
        m_parents_payments = parcel.readParcelable(PagesActivation::class.java.classLoader)!!
        m_parents_notifications = parcel.readParcelable(PagesActivation::class.java.classLoader)!!
        m_parents_monthly_calendar =
            parcel.readParcelable(PagesActivation::class.java.classLoader)!!
        m_parents_call_us = parcel.readParcelable(PagesActivation::class.java.classLoader)!!
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(m_parents_students, flags)
        parcel.writeParcelable(m_parents_events, flags)
        parcel.writeParcelable(m_parents_payments, flags)
        parcel.writeParcelable(m_parents_notifications, flags)
        parcel.writeParcelable(m_parents_monthly_calendar, flags)
        parcel.writeParcelable(m_parents_call_us, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PagesActivationResp> {
        override fun createFromParcel(parcel: Parcel): PagesActivationResp {
            return PagesActivationResp(parcel)
        }

        override fun newArray(size: Int): Array<PagesActivationResp?> {
            return arrayOfNulls(size)
        }
    }


}

