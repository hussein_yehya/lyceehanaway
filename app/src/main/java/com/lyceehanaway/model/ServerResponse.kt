package com.lyceehanaway.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ServerResponse<T> : Serializable {

    @SerializedName("data")
    var data: T? = null

    @SerializedName("statusCode")
    var statusCode: String = ""

    @SerializedName("message")
    var message: String = ""
}
