package com.lyceehanaway.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class Schedual() : Parcelable {

    @SerializedName("period_title")
    var period_title: String? = ""

    @SerializedName("subject_title")
    var subject_title: String? = ""

    @SerializedName("teacher")
    var teacher: String? = ""

    @SerializedName("startTime")
    var startTime: String? = ""

    @SerializedName("endTime")
    var endTime: String? = ""

    @SerializedName("day")
    var day: String? = ""

    constructor(parcel: Parcel) : this() {
        period_title = parcel.readString()
        subject_title = parcel.readString()
        teacher = parcel.readString()
        startTime = parcel.readString()
        endTime = parcel.readString()
        day = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(period_title)
        parcel.writeString(subject_title)
        parcel.writeString(teacher)
        parcel.writeString(startTime)
        parcel.writeString(endTime)
        parcel.writeString(day)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Schedual> {
        override fun createFromParcel(parcel: Parcel): Schedual {
            return Schedual(parcel)
        }

        override fun newArray(size: Int): Array<Schedual?> {
            return arrayOfNulls(size)
        }
    }


}

