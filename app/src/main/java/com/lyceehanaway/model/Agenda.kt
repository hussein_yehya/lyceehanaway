package com.lyceehanaway.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class Agenda() : Parcelable {

    @SerializedName("date")
    var date: String? = ""

    @SerializedName("title")
    var title: String? = ""

    @SerializedName("category")
    var category: String? = ""

    @SerializedName("body")
    var body: String? = ""

    @SerializedName("classsectionlabel")
    var classsectionlabel: String? = ""

    @SerializedName("subject")
    var subject: String? = ""

    constructor(parcel: Parcel) : this() {
        date = parcel.readString()
        title = parcel.readString()
        category = parcel.readString()
        body = parcel.readString()
        classsectionlabel = parcel.readString()
        subject = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(date)
        parcel.writeString(title)
        parcel.writeString(category)
        parcel.writeString(body)
        parcel.writeString(classsectionlabel)
        parcel.writeString(subject)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Agenda> {
        override fun createFromParcel(parcel: Parcel): Agenda {
            return Agenda(parcel)
        }

        override fun newArray(size: Int): Array<Agenda?> {
            return arrayOfNulls(size)
        }
    }

}

