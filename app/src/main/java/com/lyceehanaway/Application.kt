package com.lyceehanaway

import android.app.Application
import android.content.Context
import android.content.res.Configuration
import android.content.res.Resources
import android.os.Build
import android.util.Log
import bwabt.watan.utils.LocaleManager
import com.google.firebase.FirebaseApp
import com.lyceehanaway.utils.PrefUtils
import dagger.hilt.android.HiltAndroidApp
import java.util.*

@HiltAndroidApp
class Application : Application() {

    override fun onCreate() {
        super.onCreate()

        try {
            LocaleManager.setNewLocale(this, PrefUtils.getLanguage(this))
        } catch (e: Exception) {
        }

        try {
            FirebaseApp.initializeApp(this)
        } catch (e: Exception) {
        }

       val currentSysLocale = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Resources.getSystem().getConfiguration().locales[0].language.toString().trim()
        } else {
            "en"
        }

        val currentAppLocale = Locale.getDefault().getLanguage()
        Log.d("system_language","$currentSysLocale")
        Log.d("system_language","$currentAppLocale")


        if (PrefUtils.getLanguage(this).equals("")){
            PrefUtils.setLanguage(this, currentSysLocale.toString())
            LocaleManager.setNewLocale(this, currentSysLocale)

        }

    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(base!!))
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        LocaleManager.setLocale(this)
    }

}