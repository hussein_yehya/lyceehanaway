package com.lyceehanaway.api

import com.google.gson.JsonObject
import retrofit2.http.Body
import javax.inject.Inject

class ApiHelper @Inject constructor(private val apiService: ApiService) {
    suspend fun login(@Body body: JsonObject) = apiService.login(body)
    suspend fun getSlider(@Body body: JsonObject) = apiService.getSlider(body)
    suspend fun getSchoolEvents(@Body body: JsonObject) = apiService.getSchoolEvents(body)
    suspend fun getNotifications(@Body body: JsonObject) = apiService.getNotifications(body)
    suspend fun getAnnualCalender() = apiService.getAnnualCalender()
    suspend fun getStudentsOfParents(@Body body: JsonObject) = apiService.getStudentsOfParents(body)
    suspend fun getCalenderByDate(@Body body: JsonObject) = apiService.getCalenderByDate(body)
    suspend fun getClassSchedual(@Body body: JsonObject) = apiService.getClassSchedual(body)
    suspend fun getAbsence(@Body body: JsonObject) = apiService.getAbsence(body)
    suspend fun getAgenda(@Body body: JsonObject) = apiService.getAgenda(body)
    suspend fun getPayments(@Body body: JsonObject) = apiService.getPayments(body)
    suspend fun getParentMeetings(@Body body: JsonObject) = apiService.getParentMeetings(body)
    suspend fun getCards(@Body body: JsonObject) = apiService.getCards(body)
    suspend fun getGrades(@Body body: JsonObject) = apiService.getGrades(body)
    suspend fun logout(@Body body: JsonObject) = apiService.logout(body)
    suspend fun device(@Body body: JsonObject) = apiService.device(body)
    suspend fun aboutUs() = apiService.aboutUs()
    suspend fun getVersion() = apiService.getVersion()
}