package com.lyceehanaway.api


object ApiContract {

    interface Page {

        companion object {

            const val LOGIN = "login.php"
            const val GET_SLIDER = "getslider.php"
            const val PAYMENTS = "payments.php"
            const val NOTIFICATION_LIST = "announcements.php"
            const val STUDENT_OF_PARENTS = "students_of_parent.php"
            const val SCHOOL_EVENTS = "school_events.php"
            const val ANNUAL_CALENDER = "annual_calendar.php"
            const val AGENDA = "agenda.php"
            const val ABSENCE = "absence.php"
            const val CLASS_SCHEDUAL = "class_schedule.php"
            const val CARDS = "cards.php"
            const val GRADES = "grades.php"
            const val PARENT_MEETINGS = "parent_meetings.php"
            const val LOGOUT = "logout.php"
            const val ABOUT = "aboutus.php"
            const val DEVICE = "device.php"
            const val ANDROID_VERSION = "force_update.php"


        }
    }


    interface Param {

        companion object {

            const val user_username = "user_username"
            const val user_password = "user_password"
            const val user_token = "user_token"
            const val user_type = "user_type"
            const val servertype = "servertype"
            const val idparent = "idparent"
            const val idclass_section = "idclass_section"
            const val class_idclass = "class_idclass"
            const val id_student = "id_student"
            const val classSectionID = "classSectionID"
            const val idcard_element = "idcard_element"
            const val idcard = "idcard"
            const val date = "date"
            const val idstudent = "idstudent"
            const val idyear = "idyear"
            const val limit = "limit"
            const val offset = "offset"

            const val deviceName = "deviceName"
            const val androidVersion = "androidVersion"
            const val deviceID = "deviceID"
            const val deviceToken = "deviceToken"
            const val appVersion = "appVersion"
            const val login = "login"
            const val id = "id"
            const val type = "type"


        }
    }


}
