package com.lyceehanaway.api

import com.google.gson.JsonObject
import com.lyceehanaway.model.*
import retrofit2.http.Body
import retrofit2.http.POST


interface ApiService {

    @POST(ApiContract.Page.LOGIN)
    suspend fun login(@Body body: JsonObject): ServerResponse<User>

    @POST(ApiContract.Page.GET_SLIDER)
    suspend fun getSlider(@Body body: JsonObject): ServerResponse<SliderResp>

    @POST(ApiContract.Page.SCHOOL_EVENTS)
    suspend fun getSchoolEvents(@Body body: JsonObject): ServerResponse<ArrayList<SchoolEventResp>>

    @POST(ApiContract.Page.STUDENT_OF_PARENTS)
    suspend fun getStudentsOfParents(@Body body: JsonObject): ServerResponse<ArrayList<Student>>

    @POST(ApiContract.Page.NOTIFICATION_LIST)
    suspend fun getNotifications(@Body body: JsonObject): ServerResponse<NotificationsResp>

    @POST(ApiContract.Page.ANNUAL_CALENDER)
    suspend fun getAnnualCalender(): ServerResponse<CalenderResp>

    @POST(ApiContract.Page.ANNUAL_CALENDER)
    suspend fun getCalenderByDate(@Body body: JsonObject): ServerResponse<CalenderResp>

    @POST(ApiContract.Page.AGENDA)
    suspend fun getAgenda(@Body body: JsonObject): ServerResponse<AgendaResp>

    @POST(ApiContract.Page.ABSENCE)
    suspend fun getAbsence(@Body body: JsonObject): ServerResponse<ArrayList<Absence>>

    @POST(ApiContract.Page.CLASS_SCHEDUAL)
    suspend fun getClassSchedual(@Body body: JsonObject): ServerResponse<SchedualResp>

    @POST(ApiContract.Page.PARENT_MEETINGS)
    suspend fun getParentMeetings(@Body body: JsonObject): ServerResponse<SchedualListResp>

    @POST(ApiContract.Page.PAYMENTS)
    suspend fun getPayments(@Body body: JsonObject): ServerResponse<ArrayList<PaymentResp>>

    @POST(ApiContract.Page.CARDS)
    suspend fun getCards(@Body body: JsonObject): ServerResponse<ArrayList<Card>>

    @POST(ApiContract.Page.GRADES)
    suspend fun getGrades(@Body body: JsonObject): ServerResponse<ArrayList<GradesResp>>

    @POST(ApiContract.Page.LOGOUT)
    suspend fun logout(@Body body: JsonObject): ServerResponse<Any>

    @POST(ApiContract.Page.DEVICE)
    suspend fun device(@Body body: JsonObject): ServerResponse<Any>

    @POST(ApiContract.Page.ABOUT)
    suspend fun aboutUs(): ServerResponse<AboutUs>

    @POST(ApiContract.Page.ANDROID_VERSION)
    suspend fun getVersion(): ServerResponse<Version>


}