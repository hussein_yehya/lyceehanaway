package com.lyceehanaway.utils;

import android.app.Activity;
import android.graphics.drawable.Drawable;

import com.lyceehanaway.R;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;

import java.util.Collection;
import java.util.HashSet;

/**
 * Decorate several days with a dot
 */
public class AfterDecorator implements DayViewDecorator {

     private HashSet<CalendarDay> dates;
    private final Drawable drawable;
     public AfterDecorator(  Collection<CalendarDay> dates, Activity context) {
         this.dates = new HashSet<>(dates);
        drawable = context.getResources().getDrawable(R.drawable.red_circle);    }

    @Override
    public boolean shouldDecorate(CalendarDay day) {
        return dates.contains(day);
    }

    @Override
    public void decorate(DayViewFacade view) {
        view.setSelectionDrawable(drawable);
     }
}