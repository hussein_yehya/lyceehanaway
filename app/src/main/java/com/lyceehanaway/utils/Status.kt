package com.lyceehanaway.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}