package com.lyceehanaway.utils


interface OnReadNotification {
    fun onReadNotification(id: String)
}