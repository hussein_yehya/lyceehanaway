package com.lyceehanaway.utils

import android.annotation.SuppressLint
import android.content.Context
import androidx.preference.PreferenceManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.lyceehanaway.model.*


@SuppressLint("CommitPrefEdits")
object PrefUtils {

    val LOGGED_IN = "logged_in"
    val TOKEN = "TOKEN"
    val NAME = "NAME"
    val USER_ID = "USER_ID"
    val NOTIF_TOKEN = "NOTIF_TOKEN"
    val PHONE = "PHONE"
    val LANGUAGE = "LANGUAGE"
    val APP_VER = "APP_VER"
    val APP_BUILD = "APP_BUILD"
    val LEATEST_VER = "LEATEST_VER"
    val EMAIL = "EMAIL"
    val DEVICE_ID = "DEVICE_ID"
    val Image = "Image"
    val DEVICE_TYPE = "DEVICE_TYPE"
    val DEV_NAME = "DEV_NAME"
    val OS_VER = "OS_VER"
    val HOME_SLIDES = "HOME_SLIDES"
    val USER_TYPE = "USER_TYPE"
    val NAME_AR = "NAME_AR"
    val NAME_FR = "NAME_FR"
    val NOTIFICATIONS = "NOTIFICATIONS"
    val USERS = "USERS"
    val ABOUTUS = "ABOUTUS"
    val PAGES = "PAGES"

    fun setOsVer(context: Context, text: String) {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        sp.edit().putString(OS_VER, text).commit()
    }

    fun getOsVer(context: Context): String {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getString(OS_VER, "")!!
    }

    fun setUserType(context: Context, text: String) {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        sp.edit().putString(USER_TYPE, text).commit()
    }

    fun getUserType(context: Context): String {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getString(USER_TYPE, "")!!
    }

    fun setOSVer(context: Context, text: String) {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        sp.edit().putString(OS_VER, text).commit()
    }

    fun getOSVer(context: Context): String {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getString(OS_VER, "")!!
    }

    fun setDevName(context: Context, text: String) {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        sp.edit().putString(DEV_NAME, text).commit()
    }

    fun getDevName(context: Context): String {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getString(DEV_NAME, "")!!
    }




    fun setDeviceId(context: Context, value: String) {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        sp.edit().putString(DEVICE_ID, value).commit()
    }

    fun getDeviceId(context: Context): String {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getString(DEVICE_ID, "").toString()
    }


    fun setEmail(context: Context, value: String) {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        sp.edit().putString(EMAIL, value).commit()
    }

    fun getEmail(context: Context): String {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getString(EMAIL, "").toString()
    }

    fun setImage(context: Context, value: String) {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        sp.edit().putString(Image, value).commit()
    }

    fun getImage(context: Context): String {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getString(Image, "").toString()
    }


    fun setLoggedIn(context: Context, boolean: Boolean) {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        sp.edit().putBoolean(LOGGED_IN, boolean).commit()
    }

    fun getLoggedIn(context: Context): Boolean {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getBoolean(LOGGED_IN, false)
    }

    fun setMobile(context: Context, text: String) {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        sp.edit().putString(PHONE, text).commit()
    }

    fun getMobile(context: Context): String {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getString(PHONE, "")!!
    }

    fun setToken(context: Context, text: String) {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        sp.edit().putString(TOKEN, text).commit()
    }

    fun getToken(context: Context): String {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getString(TOKEN, "")!!
    }


    fun setNotificationToken(context: Context, text: String) {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        sp.edit().putString(NOTIF_TOKEN, text).commit()
    }

    fun getNotificationToken(context: Context): String {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getString(NOTIF_TOKEN, "")!!
    }

    fun setUserId(context: Context, text: String) {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        sp.edit().putString(USER_ID, text).commit()
    }

    fun getUserId(context: Context): String {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getString(USER_ID, "")!!
    }


    fun setName(context: Context, text: String) {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        sp.edit().putString(NAME, text).commit()
    }

    fun getName(context: Context): String {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getString(NAME, "")!!
    }
    fun setNameAr(context: Context, text: String) {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        sp.edit().putString(NAME_AR, text).commit()
    }

    fun getNameAr(context: Context): String {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getString(NAME_AR, "")!!
    }
    fun setNameFr(context: Context, text: String) {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        sp.edit().putString(NAME_FR, text).commit()
    }

    fun getNameFr(context: Context): String {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getString(NAME_FR, "")!!
    }


    fun setLanguage(context: Context, text: String) {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        sp.edit().putString(LANGUAGE, text).commit()
    }

    fun getLanguage(context: Context): String {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getString(LANGUAGE, "")!!
    }

    fun setAppVer(context: Context, text: String) {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        sp.edit().putString(APP_VER, text).commit()
    }

    fun getAppVer(context: Context): String {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getString(APP_VER, "")!!
    }

    fun setAppBuild(context: Context, text: String) {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        sp.edit().putString(APP_BUILD, text).commit()
    }

    fun getAppBuild(context: Context): String {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getString(APP_BUILD, "")!!
    }


    fun setDLeatestVer(context: Context, text: String) {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        sp.edit().putString(LEATEST_VER, text).commit()
    }

    fun getDLeatestVer(context: Context): String {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getString(LEATEST_VER, "0")!!
    }


    fun getSlides(context: Context): ArrayList<Slider> {
        val sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context)
        val gson = Gson()
        val json = sharedPrefs.getString(HOME_SLIDES, "")!!
        val type = object : TypeToken<ArrayList<Slider>>() {

        }.type
        try {
            val arrayList = gson.fromJson<ArrayList<Slider>>(json, type)

            return arrayList
        } catch (e: Exception) {
            return ArrayList<Slider>()

        }

    }


    fun saveSlides(context: Context, arrayList: ArrayList<Slider>) {
        val sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPrefs.edit()
        val gson = Gson()

        val json = gson.toJson(arrayList)

        editor.putString(HOME_SLIDES, json)
        editor.commit()
    }


    fun setNotifications(context: Context, arrayList: ArrayList<Notification>) {
        val sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPrefs.edit()
        val gson = Gson()

        val json = gson.toJson(arrayList)

        editor.putString(NOTIFICATIONS, json)
        editor.commit()
    }

    fun getNotifications(context: Context): ArrayList<Notification> {
        val sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context)
        val gson = Gson()
        val json = sharedPrefs.getString(NOTIFICATIONS, "")!!
        val type = object : TypeToken<ArrayList<Notification>>() {

        }.type
        try {
            val arrayList = gson.fromJson<ArrayList<Notification>>(json, type)

            return arrayList
        } catch (e: Exception) {
            return ArrayList<Notification>()

        }
    }

    fun setUsers(context: Context, arrayList: ArrayList<User>) {
        val sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPrefs.edit()
        val gson = Gson()

        val json = gson.toJson(arrayList)

        editor.putString(USERS, json)
        editor.commit()
    }

    fun getUsers(context: Context): ArrayList<User> {
        val sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context)
        val gson = Gson()
        val json = sharedPrefs.getString(USERS, "")!!
        val type = object : TypeToken<ArrayList<User>>() {

        }.type
        try {
            val arrayList = gson.fromJson<ArrayList<User>>(json, type)

            return arrayList
        } catch (e: Exception) {
            return ArrayList<User>()

        }
    }

    fun setPages(context: Context, arrayList: ArrayList<PagesActivation>) {
        val sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPrefs.edit()
        val gson = Gson()

        val json = gson.toJson(arrayList)

        editor.putString(PAGES, json)
        editor.commit()
    }

    fun getPages(context: Context): ArrayList<PagesActivation> {
        val sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context)
        val gson = Gson()
        val json = sharedPrefs.getString(PAGES, "")!!
        val type = object : TypeToken<ArrayList<PagesActivation>>() {

        }.type
        try {
            val arrayList = gson.fromJson<ArrayList<PagesActivation>>(json, type)

            return arrayList
        } catch (e: Exception) {
            return ArrayList<PagesActivation>()

        }
    }

    fun setAbout(context: Context, data: AboutUs) {
        val sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPrefs.edit()
        val gson = Gson()

        val json = gson.toJson(data)

        editor.putString(ABOUTUS, json)
        editor.commit()
    }

    fun getAbout(context: Context): AboutUs {
        val sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context)
        val gson = Gson()
        val json = sharedPrefs.getString(ABOUTUS, "")!!
        val type = object : TypeToken<AboutUs>() {

        }.type
        try {
            val arrayList = gson.fromJson<AboutUs>(json, type)

            return arrayList
        } catch (e: Exception) {
            return AboutUs()

        }
    }

}




