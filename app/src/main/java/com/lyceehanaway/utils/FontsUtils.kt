package com.lyceehanaway.utils

import android.content.Context
import android.graphics.Typeface
import android.widget.TextView


object FontsUtils {
    val LatoBlack = "fonts/Lato-Black.ttf"
     val LatoBold = "fonts/Lato-Bold.ttf"
    val LatoRegular = "fonts/Lato-Regular.ttf"
    val LatoSemibold = "fonts/Lato-Semibold.ttf"


    fun getLatoBlack(textView: TextView, c: Context) {
        textView.typeface = Typeface.createFromAsset(c.assets, LatoBlack)
    }
    fun getLatoBold(textView: TextView, c: Context) {
        textView.typeface = Typeface.createFromAsset(c.assets, LatoBold)
    }

    fun getLatoRegular(textView: TextView, c: Context) {
        textView.typeface = Typeface.createFromAsset(c.assets, LatoRegular)
    }

    fun getLatoSemibold(textView: TextView, c: Context) {
        textView.typeface = Typeface.createFromAsset(c.assets, LatoSemibold)
    }




}
