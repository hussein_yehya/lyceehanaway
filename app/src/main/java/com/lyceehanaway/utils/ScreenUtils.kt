package com.lyceehanaway.utils

import android.content.Context
import android.content.Context.WINDOW_SERVICE
import android.util.DisplayMetrics
import android.view.WindowManager


class ScreenUtils {

    companion object {
        fun getScreenWidthInPixels(context: Context): Int {
            var dm = DisplayMetrics()
            var windowManager = context.getSystemService(WINDOW_SERVICE) as WindowManager
            windowManager.defaultDisplay.getMetrics(dm)
            return dm.widthPixels
        }

    }
}