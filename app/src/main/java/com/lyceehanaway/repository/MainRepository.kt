package  com.lyceehanaway.repository


import com.google.gson.JsonObject
import com.lyceehanaway.api.ApiService
import com.lyceehanaway.model.*
import retrofit2.http.Body
import javax.inject.Inject

class MainRepository @Inject constructor(private val apiService: ApiService) {

    suspend fun login(@Body body: JsonObject): ServerResponse<User> {
        return apiService.login(body)
    }

    suspend fun getSlider(@Body body: JsonObject): ServerResponse<SliderResp> {
        return apiService.getSlider(body)
    }

    suspend fun getStudentsOfParents(@Body body: JsonObject): ServerResponse<ArrayList<Student>> {
        return apiService.getStudentsOfParents(body)
    }

    suspend fun getAgenda(@Body body: JsonObject): ServerResponse<AgendaResp> {
        return apiService.getAgenda(body)
    }

    suspend fun getSchoolEvents(@Body body: JsonObject): ServerResponse<ArrayList<SchoolEventResp>> {
        return apiService.getSchoolEvents(body)
    }

    suspend fun getAnnualCalender(): ServerResponse<CalenderResp> {
        return apiService.getAnnualCalender()
    }

    suspend fun getCalenderByDate(@Body body: JsonObject): ServerResponse<CalenderResp> {
        return apiService.getCalenderByDate(body)
    }

    suspend fun getAbsence(@Body body: JsonObject): ServerResponse<ArrayList<Absence>> {
        return apiService.getAbsence(body)
    }

    suspend fun getNotifications(@Body body: JsonObject): ServerResponse<NotificationsResp> {
        return apiService.getNotifications(body)
    }

    suspend fun getClassSchedual(@Body body: JsonObject): ServerResponse<SchedualResp> {
        return apiService.getClassSchedual(body)
    }

    suspend fun getPayments(@Body body: JsonObject): ServerResponse<ArrayList<PaymentResp>> {
        return apiService.getPayments(body)
    }

    suspend fun getCards(@Body body: JsonObject): ServerResponse<ArrayList<Card>> {
        return apiService.getCards(body)
    }

    suspend fun getGrades(@Body body: JsonObject): ServerResponse<ArrayList<GradesResp>> {
        return apiService.getGrades(body)
    }

    suspend fun getParentMeetings(@Body body: JsonObject): ServerResponse<SchedualListResp> {
        return apiService.getParentMeetings(body)
    }

    suspend fun logout(@Body body: JsonObject): ServerResponse<Any> {
        return apiService.logout(body)
    }

    suspend fun devicde(@Body body: JsonObject): ServerResponse<Any> {
        return apiService.device(body)
    }

    suspend fun getAbout(): ServerResponse<AboutUs> {
        return apiService.aboutUs()
    }
    suspend fun getVersion(): ServerResponse<Version> {
        return apiService.getVersion()
    }

}