package com.lyceehanaway.ui.studentEvents

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import bwabt.watan.utils.LocaleManager
import com.lyceehanaway.R
import com.lyceehanaway.utils.FontsUtils
import com.lyceehanaway.utils.PrefUtils
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_student_events.*

@AndroidEntryPoint
class StudentEventsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_student_events)
        supportActionBar?.hide()

        initFonts()
        initviews()
    }

    private fun initviews() {

        imageview_back.setOnClickListener { finish() }
        if (PrefUtils.getLanguage(this).equals("ar"))
            imageview_back.rotation = 180f
    }
    private fun initFonts() {

        FontsUtils.getLatoBlack(textview_title, this)
    }
    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(base!!))
    }

}