package com.lyceehanaway.ui.agenda


import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.google.gson.JsonObject
import com.lyceehanaway.api.ApiContract
import com.lyceehanaway.repository.MainRepository
import com.lyceehanaway.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

@HiltViewModel
class AgendaViewModel @Inject constructor(
    private val mainRepository: MainRepository,
    private val context: android.app.Application
) : ViewModel() {
    fun getAgenda(id: String,date: String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            var jsonObject: JsonObject = JsonObject()
            jsonObject.addProperty(ApiContract.Param.classSectionID, id)
            jsonObject.addProperty(ApiContract.Param.date, date)


            emit(Resource.success(data = mainRepository.getAgenda(jsonObject)))
        } catch (exception: Exception) {
            emit(Resource.error(exception.message ?: "Error Occurred!", data = null))
        }
    }
}