package com.lyceehanaway.ui.agenda

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import bwabt.watan.utils.LocaleManager
import com.lyceehanaway.R
import com.lyceehanaway.adapter.AgendaListAdapter
import com.lyceehanaway.model.Agenda
import com.lyceehanaway.utils.*
import com.prolificinteractive.materialcalendarview.CalendarDay
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_agenda.*
import java.text.SimpleDateFormat
import java.time.DayOfWeek
import java.util.*
import kotlin.collections.ArrayList

@AndroidEntryPoint
class AgendaActivity : AppCompatActivity() {

    private val viewModel: AgendaViewModel by viewModels()

    var id = ""
    var date = ""

    var currDateLoaded = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_agenda)
        supportActionBar?.hide()
        initFonts()
        initViews()

    }

    private fun initViews() {

        try {
            id = intent.getStringExtra("id").toString()
        } catch (e: Exception) {
        }

        imageview_back.setOnClickListener { finish() }

        if (PrefUtils.getLanguage(this).equals("ar"))
            imageview_back.rotation = 180f


        calendarView.setOnDateChangedListener { widget, date, selected ->

            var month:String = date.month.toString()
            if (month.toString().length==1)
                month = "0$month"

            var day:String = date.day.toString()
            if (day.toString().length==1)
                day = "0$day"

              this.date = "${date.year}-${month}-${day}"
            getAgenda(this.date)
         }




       getAgenda("")

    }

    private fun initFonts() {
        FontsUtils.getLatoBlack(textview_title, this)
    }


    private fun showAgenda(list: ArrayList<Agenda>) {

        recyclerview_agenda.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        var adapter =
            AgendaListAdapter(list, object : AgendaListAdapter.ItemClickListener {
                override fun onItemClick(item: Agenda) {


                }
            }
            )

        recyclerview_agenda.adapter = adapter
    }


    private fun getAgenda(d:String) {


        viewModel.getAgenda(id,d).observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    progressbar.visibility = View.GONE


                    if (!currDateLoaded){
                        val c = Calendar.getInstance().time
                        val df = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                        val formattedDate: String = df.format(c)
                        date = formattedDate

                        currDateLoaded = true

                        showDates(it.data!!.data!!.agendaDates)

                        calendarView.setSelectedDate(CalendarDay.today())

                    }else{
                        showAgenda(it.data!!.data!!.agenda)

                    }



                }
                Status.LOADING -> {
                    progressbar.visibility = View.VISIBLE
                }
                Status.ERROR -> {
                    //Handle Error
                    progressbar.visibility = View.GONE
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
            }
        })

    }

    private fun showDates(calendarDates: ArrayList<String>) {

        var datesBef = ArrayList<CalendarDay>()
        var datesAft = ArrayList<CalendarDay>()
        var datesNow = ArrayList<CalendarDay>()

        for (date in calendarDates){

            Log.d("date__",date)
            var d = date.substringBefore("-")
            var y = date.substringAfterLast("-")
            var m = date.substringBeforeLast("-").substringAfterLast("-")
            Log.d("date__y",y)
            Log.d("date__d",d)
            Log.d("date__m",m)

            var cd= CalendarDay.from(y.toInt(),m.toInt(),d.toInt())
            if (cd.isBefore(CalendarDay.today())){
                datesBef.add(cd)
                }

            if (cd.isAfter(CalendarDay.today())){
                datesAft.add(cd)
                }
            if (cd==CalendarDay.today()){
                datesNow.add(cd)
             }

            calendarView.setSelectedDate(cd)

        }
         calendarView.addDecorators(
             BeforeDecorator(datesBef,this),
             TodayDecorator(datesNow,this),
             AfterDecorator(datesAft,this)
         )


    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(base!!))
    }

}