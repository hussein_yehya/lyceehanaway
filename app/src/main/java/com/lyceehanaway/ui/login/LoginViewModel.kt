package com.lyceehanaway.ui.login

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.google.gson.JsonObject
import com.lyceehanaway.api.ApiContract
import com.lyceehanaway.repository.MainRepository
import com.lyceehanaway.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val mainRepository: MainRepository
) : ViewModel() {
    fun login(username:String,password:String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            var jsonObject: JsonObject = JsonObject()
            jsonObject.addProperty(ApiContract.Param.user_username, username)
            jsonObject.addProperty(ApiContract.Param.user_password, password)
            jsonObject.addProperty(ApiContract.Param.servertype, "1")

            emit(Resource.success(data = mainRepository.login(jsonObject)))
        } catch (exception: Exception) {
            emit(Resource.error(exception.message ?: "Error Occurred!", data = null))
        }
    }
}