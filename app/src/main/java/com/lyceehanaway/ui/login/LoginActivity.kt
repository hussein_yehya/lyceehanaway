package com.lyceehanaway.ui.login

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import bwabt.watan.utils.LocaleManager
import com.lyceehanaway.R
import com.lyceehanaway.model.ServerResponse
import com.lyceehanaway.model.User
import com.lyceehanaway.ui.home.HomeActivity
import com.lyceehanaway.utils.FontsUtils
import com.lyceehanaway.utils.PrefUtils
import com.lyceehanaway.utils.Status
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_login.*


@AndroidEntryPoint
class LoginActivity : AppCompatActivity() {

    private val viewModel: LoginViewModel by viewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        supportActionBar?.hide()

        initFonts()
        initViews()
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun initViews() {


        button_signin.setOnClickListener {
            login()
        }

        imageview_eye.setOnTouchListener(OnTouchListener { view, motionEvent ->
            when (motionEvent.action) {
                MotionEvent.ACTION_DOWN -> edittext_password.setInputType(InputType.TYPE_CLASS_TEXT)
                MotionEvent.ACTION_UP -> edittext_password.setInputType(InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD)
            }
            true
        })

    }

    private fun login() {

        var username = edittext_username.text.toString().trim()
        var password = edittext_password.text.toString().trim()

        if (username.isEmpty() || password.isEmpty())
            return

        viewModel.login(username, password).observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    progressBar.visibility = View.GONE
                    button_signin.visibility = View.VISIBLE
                    it.data?.let { usersData -> saveUser(usersData, username, password) }


//                    Toast.makeText(this, it.data!!.data!!.name_en, Toast.LENGTH_LONG).show()


                }
                Status.LOADING -> {
                    progressBar.visibility = View.VISIBLE
                    button_signin.visibility = View.GONE
                }
                Status.ERROR -> {
                    //Handle Error
                    progressBar.visibility = View.GONE
                    button_signin.visibility = View.VISIBLE
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
            }
        })


    }

    private fun saveUser(usersData: ServerResponse<User>, username: String, password: String) {

        if (PrefUtils.getUsers(this).size == 0) {
            var users = ArrayList<User>()
            users.add(User(username, password, usersData.data!!.fullname.toString()))
            PrefUtils.setUsers(this, users)
        } else {
            var users = PrefUtils.getUsers(this)

            var found = false

            for (u in users) {
                if (u.username.toString().equals(username)) {
                    u.password = password
                    u.fullname = usersData.data!!.fullname.toString()
                    found = true
                }
            }

            if (!found)
                users.add(User(username, password, usersData.data!!.fullname.toString()))

            PrefUtils.setUsers(this, users)

        }

        PrefUtils.setLoggedIn(this, true)
        PrefUtils.setUserId(this, usersData.data!!.id.toString())
        PrefUtils.setName(this, usersData.data!!.fullname.toString())
        PrefUtils.setUserType(this, usersData.data!!.type.toString())
        PrefUtils.setToken(this, usersData.data!!.token.toString())
        PrefUtils.setPages(this, usersData.data!!.pages)
        startActivity(Intent(this@LoginActivity, HomeActivity::class.java))
        finish()
    }

    private fun initFonts() {
        FontsUtils.getLatoRegular(button_signin, this)
        FontsUtils.getLatoRegular(edittext_password, this)
        FontsUtils.getLatoRegular(edittext_username, this)
        FontsUtils.getLatoBold(textview_signin, this)
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(base!!))
    }

}