package com.lyceehanaway.ui.home

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import bwabt.watan.utils.LocaleManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.lyceehanaway.BuildConfig
import com.lyceehanaway.R
import com.lyceehanaway.databinding.ActivityHomeBinding
import com.lyceehanaway.ui.dialog.AgreementDialog
import com.lyceehanaway.ui.notifications.NotificationsActivity
import com.lyceehanaway.utils.FontsUtils
import com.lyceehanaway.utils.PrefUtils
import com.lyceehanaway.utils.Status
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_home.*

@AndroidEntryPoint
class HomeActivity : AppCompatActivity() {

    private lateinit var binding: ActivityHomeBinding

    private val viewModel: MainViewModel by viewModels()

    companion object {
        lateinit var activity: HomeActivity

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        activity = this

        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initFonts()
        val navView: BottomNavigationView = binding.navView

        val navController = findNavController(R.id.nav_host_fragment_activity_home)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_home,
//                R.id.navigation_dashboard,
//                R.id.navigation_notifications,
                R.id.navigation_settings
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        registerDevice()
        getNotifications()
       getVersion()
        image_notif.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    NotificationsActivity::class.java
                )
            )
        }
    }

    private fun registerDevice() {

        viewModel.device("1").observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {

                }
                Status.LOADING -> {

                }
                Status.ERROR -> {
                    Log.d("Awdawdawd1",it.message.toString())

                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
            }
        })

    }

    private fun initFonts() {
        FontsUtils.getLatoBlack(textview_title, this)
    }


    fun setNewLocale(mContext: AppCompatActivity, @LocaleManager.LocaleDef language: String) {
        LocaleManager.setNewLocale(this, language)
        val intent = mContext.intent
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(base!!))
    }


    private fun getNotifications() {
        viewModel.getNotifications("50", "0")
            .observe(this, Observer {
                when (it.status) {
                    Status.SUCCESS -> {

                        try {
                            PrefUtils.setNotifications(this, it.data!!.data!!.notifications)
                            if (it.data!!.data!!.count.equals("0")) {
                                textview_num.visibility = View.GONE
                            } else {
                                textview_num.visibility = View.VISIBLE
                                textview_num.text = it.data!!.data!!.count

                            }
                        } catch (e: Exception) {
                        }


                    }
                    Status.LOADING -> {
//                        progressbar.visibility = View.VISIBLE
                    }
                    Status.ERROR -> {
                        Log.d("Awdawdawd",it.message.toString())
                        //Handle Error
//                        progressbar.visibility = View.GONE
                        Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                    }
                }
            })

    }


    private fun getVersion() {
        viewModel.getVersion()
            .observe(this, Observer {
                when (it.status) {
                    Status.SUCCESS -> {

                        PrefUtils.setDLeatestVer(this,it.data!!.data!!.version!!)

                        val versionName: String = BuildConfig.VERSION_NAME
                        val verName = versionName.replace(".", "").toInt()

                        val ver: Int = it.data!!.data!!.version!!.replace(".", "", true).toInt()
                        if (verName < ver) {

                            showUpdateDialog(it.data!!.data!!.forceUpdate.toString())
                        }

                    }

                    Status.LOADING -> {
//                        progressbar.visibility = View.VISIBLE
                    }
                    Status.ERROR -> {
                        //Handle Error
//                        progressbar.visibility = View.GONE
//                        Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                    }
                }
            })

    }



    private fun showUpdateDialog(force_update:String) {

        var no = ""
        if (force_update.equals("1")){
            no = getString(R.string.cancel)
        }


        var dialog = AgreementDialog(
            this, object : AgreementDialog.OnAcceptClickListener {
                override fun onAcceptClickListener() {

                    val appPackageName: String = packageName

                    try {
                        startActivity(
                            Intent(
                                Intent.ACTION_VIEW,
                                Uri.parse("market://details?id=$appPackageName")
                            )
                        )
                    } catch (anfe: ActivityNotFoundException) {
                        startActivity(
                            Intent(
                                Intent.ACTION_VIEW,
                                Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                            )
                        )
                    }

                }

            },
            getString(R.string.update),    getString(R.string.txt_update),  getString(R.string.update),  no)

        try {
            dialog.show()
        } catch (e: Exception) {
        }

    }
}