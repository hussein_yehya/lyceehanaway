package com.lyceehanaway.ui.home.ui.home

import android.app.Application
import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.google.gson.JsonObject
import com.lyceehanaway.api.ApiContract
import com.lyceehanaway.repository.MainRepository
import com.lyceehanaway.utils.PrefUtils
import com.lyceehanaway.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val mainRepository: MainRepository,
    private val context: Application
) : ViewModel() {
    fun getSlider() = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            var jsonObject: JsonObject = JsonObject()
            jsonObject.addProperty(ApiContract.Param.user_type, PrefUtils.getUserType(context))
//            jsonObject.addProperty(ApiContract.Param.user_type, "7")
            jsonObject.addProperty(ApiContract.Param.user_token, PrefUtils.getToken(context))

            Log.d("object_resp",jsonObject.toString())
            emit(Resource.success(data = mainRepository.getSlider(jsonObject)))
        } catch (exception: Exception) {
            emit(Resource.error(exception.message ?: "Error Occurred!", data = null))
        }
    }
}