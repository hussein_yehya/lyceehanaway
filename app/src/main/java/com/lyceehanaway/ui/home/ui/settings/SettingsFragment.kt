package com.lyceehanaway.ui.home.ui.settings

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.lyceehanaway.R
import com.lyceehanaway.model.ServerResponse
import com.lyceehanaway.model.User
import com.lyceehanaway.ui.about.AboutActivity
import com.lyceehanaway.ui.changePassword.ChangePasswordActivity
import com.lyceehanaway.ui.dialog.AccountsDialog
import com.lyceehanaway.ui.dialog.AgreementDialog
import com.lyceehanaway.ui.dialog.LanguageDialog
import com.lyceehanaway.ui.dialog.LoginDialog
import com.lyceehanaway.ui.home.HomeActivity
import com.lyceehanaway.ui.login.LoginActivity
import com.lyceehanaway.utils.FontsUtils
import com.lyceehanaway.utils.PrefUtils
import com.lyceehanaway.utils.Status
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_settings.*

@AndroidEntryPoint
class SettingsFragment : Fragment() {

    lateinit var loginDialog:LoginDialog
    lateinit var acountsDialog:AccountsDialog
    private val viewModel: SettingsViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_settings, container, false)

        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initFonts()
        initViews()
    }

    private fun initViews() {

        try {
            val pInfo = requireContext().packageManager.getPackageInfo(requireContext().packageName, 0)
            val version = pInfo.versionName
            val versionCode = pInfo.versionCode
            textview_version.text = "V$version"
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        if (PrefUtils.getLanguage(requireContext()).equals("en")){
            textview_lang.text = "English"
        }else
        if (PrefUtils.getLanguage(requireContext()).equals("ar")){
            textview_lang.text = "العربية"
        }

        button_change_password.setOnClickListener {
            startActivity(Intent(requireContext(), ChangePasswordActivity::class.java))
        }

        button_aboutus.setOnClickListener {
            startActivity(Intent(requireContext(), AboutActivity::class.java))
        }

        button_logout.setOnClickListener {
            showLogoutDialog()
        }
        button_swich_account.setOnClickListener {
            showSwichDialog()
        }

        button_language.setOnClickListener {
            showLanguageDialog()
        }

        textview_lang.setOnClickListener {
            button_language.callOnClick()
        }
    }

    private fun showLanguageDialog() {

        var dialog = LanguageDialog(requireContext(),object : LanguageDialog.OnAcceptClickListener{
            override fun onAcceptClickListener(lang: String) {

                PrefUtils.setLanguage(requireContext(),lang)

                try {
                    if (HomeActivity.activity != null)
                        HomeActivity.activity.setNewLocale(
                            (activity as AppCompatActivity?)!!,
                            PrefUtils.getLanguage(requireContext())
                        )
                } catch (e: Exception) {
                }

            }

        })

        try {
            dialog.show()
        }catch (e:Exception){}
    }

    private fun showSwichDialog() {

        if (PrefUtils.getUsers(requireContext()).size <= 1) {
            loginDialog = LoginDialog(requireContext(), object : LoginDialog.OnAcceptClickListener {
                override fun onAcceptClickListener(username: String, password: String) {

                    login(username, password)

                }

            })

            try {
                loginDialog.show()
            } catch (e: java.lang.Exception) {
            }
        }else{
            acountsDialog= AccountsDialog(requireContext(),object : AccountsDialog.OnAcceptClickListener{
                override fun onAcceptClickListener(user: User) {

                    login(user.username.toString(), user.password.toString())

                }

            })
            try {
                acountsDialog.show()
            }catch (e:java.lang.Exception){}
        }
    }


      private fun login(username:String,password:String) {

        if (username.isEmpty() || password.isEmpty())
            return

        viewModel.login(username, password).observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {

                    try {
                        loginDialog.dismiss()
                    }catch (e:java.lang.Exception){}

                    try {
                        acountsDialog.dismiss()
                    }catch (e:java.lang.Exception){}

                    it.data?.let { usersData -> saveUser(usersData,username, password) }


//                    Toast.makeText(this, it.data!!.data!!.name_en, Toast.LENGTH_LONG).show()


                }
                Status.LOADING -> {
                    try {
                        loginDialog.showProgress(true)
                    }catch (e:java.lang.Exception){}
                    try {
                        acountsDialog.showProgresss(true)
                    }catch (e:java.lang.Exception){}
                }
                Status.ERROR -> {
                    try {
                        loginDialog.showProgress(false)
                    }catch (e:java.lang.Exception){}
                    try {
                        acountsDialog.showProgresss(false)
                    }catch (e:java.lang.Exception){}
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_LONG).show()
                }
            }
        })


    }

    private fun saveUser(usersData: ServerResponse<User>,username:String,password:String) {

        if (PrefUtils.getUsers(requireContext()).size == 0) {
            var users = ArrayList<User>()
            users.add(User(username, password, usersData.data!!.fullname.toString()))
            PrefUtils.setUsers(requireContext(), users)
        } else {
            var users = PrefUtils.getUsers(requireContext())

            var found = false

            for (u in users) {
                if (u.username.toString().equals(username)) {
                    u.password = password
                    u.fullname = usersData.data!!.fullname.toString()
                    found = true
                }
            }

            if (!found)
                users.add(User(username, password, usersData.data!!.fullname.toString()))

            PrefUtils.setUsers(requireContext(), users)

        }

        PrefUtils.setLoggedIn(requireContext(), true)
        PrefUtils.setUserId(requireContext(), usersData.data!!.id.toString())
        PrefUtils.setName(requireContext(), usersData.data!!.fullname.toString())
        PrefUtils.setUserType(requireContext(), usersData.data!!.type.toString())
        PrefUtils.setToken(requireContext(), usersData.data!!.token.toString())
        PrefUtils.setPages(requireContext(), usersData.data!!.pages)

        startActivity(Intent(requireContext(), HomeActivity::class.java))
        requireActivity().finishAffinity()
    }


    private fun initFonts() {

        FontsUtils.getLatoBlack(button_aboutus, requireContext())
        FontsUtils.getLatoBlack(button_swich_account, requireContext())
        FontsUtils.getLatoBlack(button_change_password, requireContext())
        FontsUtils.getLatoBlack(button_logout, requireContext())
        FontsUtils.getLatoBlack(button_language, requireContext())
        FontsUtils.getLatoRegular(textview_lang, requireContext())
        FontsUtils.getLatoRegular(textview_version, requireContext())
    }


    private fun showLogoutDialog() {

        var dialog = AgreementDialog(
            requireContext(), object : AgreementDialog.OnAcceptClickListener {
                override fun onAcceptClickListener() {
                    PrefUtils.setToken(requireContext(),"")
                    PrefUtils.setName(requireContext(),"")
                    PrefUtils.setUserId(requireContext(),"")
                    PrefUtils.setLoggedIn(requireContext(),false)
                    startActivity(Intent(requireContext(), LoginActivity::class.java) )
                    requireActivity().finishAffinity()


                }

            },
            getString(R.string.logout),    getString(R.string.logout_approve),  getString(R.string.logout),  getString(R.string.cancel)
        )

        try {
            dialog.show()
        } catch (e: Exception) {
        }

    }


}