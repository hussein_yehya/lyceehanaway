package com.lyceehanaway.ui.home.ui.home

import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.ViewPager
import com.lyceehanaway.R
import com.lyceehanaway.adapter.MenuListAdapter
import com.lyceehanaway.adapter.SliderPagerAdapter
import com.lyceehanaway.model.Menu
import com.lyceehanaway.model.PagesActivation
import com.lyceehanaway.model.Slider
import com.lyceehanaway.ui.annualCalender.AnnualCalenderActivity
import com.lyceehanaway.ui.calender.CalenderActivity
import com.lyceehanaway.ui.events.EventsActivity
import com.lyceehanaway.ui.notifications.NotificationsActivity
import com.lyceehanaway.ui.payments.PaymentsActivity
import com.lyceehanaway.ui.students.StudentsActivity
import com.lyceehanaway.utils.PrefUtils
import com.lyceehanaway.utils.Status
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_home.*

@AndroidEntryPoint
class HomeFragment : Fragment() {

    private val viewModel: HomeViewModel by viewModels()

    var page = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_home, container, false)

        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initViews()
    }

    private fun initViews() {
        initViewPager()
        showMenu()

        getSlider()
    }

    private fun getSlider() {

        viewModel.getSlider().observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {

                    PrefUtils.saveSlides(requireContext(),it.data!!.data!!.slider!!)
                    PrefUtils.setPages(requireContext(),it.data.data!!.pages!!)

                     initViewPager()

                   showMenu()

                }
                Status.LOADING -> {

                }
                Status.ERROR -> {
                    Log.d("error_",it.message.toString())

                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_LONG).show()
                }
            }
        })

        }



    private fun showMenu() {


        var list = ArrayList<Menu>()

        for(item in PrefUtils.getPages(requireContext())){
            when(item.code){
                "m_parents_students"->{
                    list.add(Menu(1, R.drawable.users, item.title.toString()))

                }
                "m_parents_events"->{
                    list.add(Menu(2, R.drawable.calendar, item.title.toString()))

                }
                "m_parents_payments"->{
                    list.add(Menu(3, R.drawable.wallet,item.title.toString()))

                }
                "m_parents_notifications"->{
                    list.add(Menu(4, R.drawable.notification, item.title.toString()))

                }
                "m_parents_monthly_calendar"->{
                    list.add(Menu(5, R.drawable.calendar2, item.title.toString()))
                }
                "m_parents_call_us" -> {
                    list.add(Menu(6, R.drawable.call, item.title.toString()))
                }
            }

        }


        try {

            val mLayoutManager =
                GridLayoutManager(requireContext(), 3, LinearLayoutManager.VERTICAL, false)


            recyclerview_menu.layoutManager = mLayoutManager

            var adapter =
                MenuListAdapter(list, object : MenuListAdapter.ItemClickListener {
                    override fun onItemClick(item: Menu) {


                        when (item.id) {


                            1 -> {
                                startActivity(
                                    Intent(
                                        requireContext(),
                                        StudentsActivity::class.java
                                    )
                                )

                            }
                            2 -> {
                                startActivity(Intent(requireContext(), EventsActivity::class.java))

                            }
                            3 -> {
                                startActivity(
                                    Intent(
                                        requireContext(),
                                        PaymentsActivity::class.java
                                    )
                                )

                            }
                            4 -> {
                                startActivity(
                                    Intent(
                                        requireContext(),
                                        NotificationsActivity::class.java
                                    )
                                )
                            }
                            5 -> {
                                startActivity(
                                    Intent(
                                        requireContext(),
                                        AnnualCalenderActivity::class.java
                                    )
                                )
                            }
                            6 -> {
                                val intent = Intent(Intent.ACTION_DIAL)
                                intent.data = Uri.parse("tel:" + "07430404")
                                startActivity(intent)
                            }
                        }

                    }


                })
            recyclerview_menu.adapter = adapter
        } catch (e: Exception) {
        }

    }


    private fun initViewPager() {


        val sliderPagerAdapter = SliderPagerAdapter(requireActivity(), PrefUtils.getSlides(requireContext()))
        viewpager.adapter = sliderPagerAdapter


        viewpager.addOnPageChangeListener(object :
            ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                addBottomDots(position, ll_dots)
                page = position
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })
        addBottomDots(0, ll_dots)

//        val h = Handler()
//        h.postDelayed(object : Runnable {
//            override fun run() {
//                if (sliderPagerAdapter.count == page) {
//                    page = 0
//                } else {
//                    page++
//                }
//                try {
//                    viewpager.currentItem = page
//                } catch (e: Exception) {
//                }
//                h.postDelayed(this, 5000)
//            }
//        }, 1000)

    }


    //showing dots on screen
    private fun addBottomDots(currentPage: Int, ll_dots: LinearLayout) {
        val dots = arrayOfNulls<TextView>(PrefUtils.getSlides(requireContext()).size)
        ll_dots.removeAllViews()
        for (i in dots.indices) {
            dots[i] = TextView(requireContext())
            dots[i]!!.text = Html.fromHtml("&#8226;")
            dots[i]!!.textSize = 50f
            dots[i]!!.setTextColor(Color.parseColor("#CDCDCD"))
            ll_dots.addView(dots[i])
        }
        if (dots.size > 0) dots[currentPage]!!.setTextColor(Color.parseColor("#6D9FEA"))
    }

}