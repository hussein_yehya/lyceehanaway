package com.lyceehanaway.ui.eventDetails

import android.Manifest
import android.app.DownloadManager
import android.app.ProgressDialog
import android.content.Context
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.text.method.LinkMovementMethod
import android.text.util.Linkify
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import bwabt.watan.utils.LocaleManager
import com.bumptech.glide.Glide
import com.lyceehanaway.R
import com.lyceehanaway.adapter.ImagesListAdapter
import com.lyceehanaway.model.SchoolEvent
import com.lyceehanaway.ui.dialog.ImageDialog
import com.lyceehanaway.ui.events.EventsViewModel
import com.lyceehanaway.utils.FontsUtils
import com.lyceehanaway.utils.Status
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_event_details.*
import kotlinx.coroutines.*
import java.io.File
import java.util.*
import androidx.lifecycle.Observer
import com.lyceehanaway.utils.PrefUtils

@AndroidEntryPoint
class EventDetailsActivity : AppCompatActivity() {

    private val viewModel: EventDetailsViewModel by viewModels()

     val PERMISSION_WRITE = 0
    var progressDialog: ProgressDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_details)
        supportActionBar?.hide()

        initFonts()
        initViews()
    }

    private fun initFonts() {
        FontsUtils.getLatoBlack(textview_title, this)
        FontsUtils.getLatoBold(textview_title1, this)
        FontsUtils.getLatoRegular(textview_date, this)
        FontsUtils.getLatoRegular(textview_body, this)
    }

    private fun initViews() {

        imageview_back.setOnClickListener { finish() }
        progressDialog = ProgressDialog(this)
        try {
           var  event:SchoolEvent = intent.getParcelableExtra("event")!!
            showEvent(event)
        } catch (e: Exception) {
            var id = intent.getStringExtra("id").toString()
            getEvents(id)
        }

        if (PrefUtils.getLanguage(this).equals("ar"))
            imageview_back.rotation = 180f

    }

    private fun showEvent(event: SchoolEvent?) {

        textview_title.text = event!!.title
        textview_title1.text = event!!.title
        textview_body.text = event!!.description
        textview_date.text = event!!.date

        textview_body.movementMethod = LinkMovementMethod.getInstance()
        Linkify.addLinks(textview_body, Linkify.WEB_URLS or Linkify.PHONE_NUMBERS)

        imageview.setOnClickListener {
            showImageDialog(event!!.photo_url.toString())
        }

        if (event!!.photo_url.equals("")) {
            imageview.visibility = View.GONE
        } else {
            imageview.visibility = View.VISIBLE
            try {
                Glide.with(this).load(event!!.photo_url.toString()).into(imageview)
            } catch (e: Exception) {
            }
        }

        showImages(event)

    }

    private fun showImages(event:SchoolEvent) {

        val mLayoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)


        recyclerview_images.layoutManager = mLayoutManager

        var adapter =
            ImagesListAdapter(event!!.images, object : ImagesListAdapter.ItemClickListener {
                override fun onItemClick(item: String) {

                    showImageDialog(item)

                }

            }
            )

        recyclerview_images.adapter = adapter
    }

    private fun showImageDialog(item: String) {

        var dialog = ImageDialog(this, object : ImageDialog.OnAcceptClickListener {
            override fun onAcceptClickListener(image: String) {


                if (checkPermission()) {
                    downloadImageNew(
                        "${Calendar.getInstance().timeInMillis}.jpg", image
                    )
                }
            }

        }, item)

        try {
            dialog.show()
        } catch (e: java.lang.Exception) {
        }
    }


    fun checkPermission(): Boolean {
        val READ_EXTERNAL_PERMISSION =
            ContextCompat.checkSelfPermission(
                this@EventDetailsActivity,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
        if (READ_EXTERNAL_PERMISSION != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                this@EventDetailsActivity,
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                PERMISSION_WRITE
            )
            return false
        }
        return true
    }


    private fun downloadImageNew(filename: String, downloadUrlOfImage: String) {
        try {
            val dm = getSystemService(DOWNLOAD_SERVICE) as DownloadManager
            val downloadUri = Uri.parse(downloadUrlOfImage)
            val request = DownloadManager.Request(downloadUri)
            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI or DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false)
                .setTitle(filename)
                .setMimeType("image/jpeg") // Your file type. You can use this code to download other file types also.
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                .setDestinationInExternalPublicDir(
                    Environment.DIRECTORY_PICTURES,
                    File.separator + filename + ".jpg"
                )
            dm.enqueue(request)
            Toast.makeText(this, "Image download started.", Toast.LENGTH_SHORT).show()
        } catch (e: java.lang.Exception) {
            Toast.makeText(this, "Image download failed.", Toast.LENGTH_SHORT).show()
        }
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(base!!))
    }

        private fun getEvents(id:String) {

        viewModel.getSchoolEvents(id).observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {

                    showEvent(it.data!!.data!![0].events[0])


                }
                Status.LOADING -> {
                 }
                Status.ERROR -> {
                    //Handle Error
                     Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
            }
        })

    }

}