package com.lyceehanaway.ui.dialog

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.recyclerview.widget.LinearLayoutManager

import com.lyceehanaway.R
import com.lyceehanaway.adapter.CardElementsListAdapter
import com.lyceehanaway.adapter.CardsListAdapter
import com.lyceehanaway.model.Card
import com.lyceehanaway.model.CardElement
import com.lyceehanaway.utils.FontsUtils
import com.lyceehanaway.utils.ScreenUtils
import kotlinx.android.synthetic.main.card_elements_dialog.*


class CardElementsDialog(
    context: Context,
    private val card:Card,
    private val onAcceptClickListener: OnAcceptClickListener
) : Dialog(context) {

    private var onClickListener: OnAcceptClickListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.card_elements_dialog)
        setCancelable(false)
        this.onClickListener = onAcceptClickListener

        initFonts()


        window?.setLayout(
            (ScreenUtils.getScreenWidthInPixels(context) * 85) / 100,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        this.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))


        val mLayoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)


        textview_title.text = card.cardTitle.toString()
        recyclerview_cards.layoutManager = mLayoutManager

        var adapter =
            CardElementsListAdapter(card.cardElements, object : CardElementsListAdapter.ItemClickListener {
                override fun onItemClick(item: CardElement) {
                    if (onAcceptClickListener != null){
                        onAcceptClickListener?.onAcceptClickListener(item)

                        try {
                            dismiss()
                        }catch (e:Exception){}
                    }

                }
            }
            )

        recyclerview_cards.adapter = adapter

        iamgeview_close.setOnClickListener {

            try {
            dismiss()
        }catch (e:Exception){} }

    }

    private fun initFonts() {

        FontsUtils.getLatoBold(textview_title, context)


    }


    interface OnAcceptClickListener {
        fun onAcceptClickListener(card:CardElement)
    }


}
