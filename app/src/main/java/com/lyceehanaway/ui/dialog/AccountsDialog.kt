package com.lyceehanaway.ui.dialog

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.recyclerview.widget.LinearLayoutManager

import com.lyceehanaway.R
import com.lyceehanaway.adapter.CardsListAdapter
import com.lyceehanaway.adapter.UsersModelListAdapter
import com.lyceehanaway.model.Card
import com.lyceehanaway.model.User
import com.lyceehanaway.utils.FontsUtils
import com.lyceehanaway.utils.PrefUtils
import com.lyceehanaway.utils.ScreenUtils
import kotlinx.android.synthetic.main.accounts_dialog.*


class AccountsDialog(
    context: Context,
    private val onAcceptClickListener: OnAcceptClickListener
) : Dialog(context) {

    private var onClickListener: OnAcceptClickListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.accounts_dialog)
        setCancelable(false)
        this.onClickListener = onAcceptClickListener

        initFonts()


        window?.setLayout(
            (ScreenUtils.getScreenWidthInPixels(context) * 85) / 100,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        this.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))


        val mLayoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)


        recyclerview_accounts.layoutManager = mLayoutManager

        var adapter =
            UsersModelListAdapter(PrefUtils.getUsers(context), object : UsersModelListAdapter.ItemClickListener {
                override fun onItemClicked(user: User) {


                    if (onAcceptClickListener != null) {
                        onAcceptClickListener?.onAcceptClickListener(user)
                        try {
                            dismiss()
                        } catch (e: Exception) {
                        }
                    }
                }
            }
            )

        recyclerview_accounts.adapter = adapter

        iamgeview_close.setOnClickListener {
            try {
                dismiss()
            } catch (e: Exception) {
            }
        }

    }

    private fun initFonts() {

        FontsUtils.getLatoBold(textview_title, context)


    }


    interface OnAcceptClickListener {
        fun onAcceptClickListener(card: User)
    }

    fun showProgresss(show:Boolean){
        if (show)
            progressbar.visibility = View.VISIBLE
        else
            progressbar.visibility = View.GONE

    }

}
