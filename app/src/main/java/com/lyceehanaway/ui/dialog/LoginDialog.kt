package com.lyceehanaway.ui.dialog

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager

import com.lyceehanaway.R
import com.lyceehanaway.utils.FontsUtils
import com.lyceehanaway.utils.ScreenUtils
import kotlinx.android.synthetic.main.login_dialog.*


class LoginDialog(
    context: Context,
    private val onAcceptClickListener: OnAcceptClickListener,
) : Dialog(context) {

    private var onClickListener: OnAcceptClickListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.login_dialog)
        setCancelable(false)
        this.onClickListener = onAcceptClickListener

        initFonts()


        window?.setLayout(
            (ScreenUtils.getScreenWidthInPixels(context) * 85) / 100,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        this.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))


        button_signin.setOnClickListener {

            var username = edittext_username.text.toString().trim()
            var password = edittext_password.text.toString().trim()

            if (onAcceptClickListener != null)
                onAcceptClickListener.onAcceptClickListener(username,password)

        }

        button_cancel.setOnClickListener {
            try {
                dismiss()
            } catch (e: Exception) {
            }
        }


    }

    private fun initFonts() {

        FontsUtils.getLatoBold(button_signin, context)
        FontsUtils.getLatoRegular(edittext_password, context)
        FontsUtils.getLatoRegular(edittext_username, context)
        FontsUtils.getLatoSemibold(textview_signin, context)
        FontsUtils.getLatoSemibold(button_cancel, context)

    }


    interface OnAcceptClickListener {
        fun onAcceptClickListener(username:String,password:String)
    }

    fun showProgress(show:Boolean){
        if (show){
            button_signin.visibility = View.GONE
            progressBar.visibility = View.VISIBLE
        }else{
            button_signin.visibility = View.VISIBLE
            progressBar.visibility = View.GONE
        }
    }

}
