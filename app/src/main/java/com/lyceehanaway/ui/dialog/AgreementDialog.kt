package com.lyceehanaway.ui.dialog

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager

import com.lyceehanaway.R
import com.lyceehanaway.utils.FontsUtils
import com.lyceehanaway.utils.ScreenUtils
import kotlinx.android.synthetic.main.agree_dialog.*


class AgreementDialog(
    context: Context,
    private val onAcceptClickListener: OnAcceptClickListener,
    var title: String,
    var body: String,
    var buttonYes: String,
    var buttonNo: String
) : Dialog(context) {

    private var onClickListener: OnAcceptClickListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.agree_dialog)
        setCancelable(false)
        this.onClickListener = onAcceptClickListener

        initFonts()

        textview_title.text = title
        textview_body.text = body
        button_yes.text = buttonYes
        button_no.text = buttonNo

        if(buttonNo.equals(""))
            button_no.visibility = View.GONE

        window?.setLayout(
            (ScreenUtils.getScreenWidthInPixels(context) * 85) / 100,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        this.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))


        button_yes.setOnClickListener {
            if (onAcceptClickListener != null)
                onAcceptClickListener.onAcceptClickListener()

        }

        button_no.setOnClickListener {
            try {
                dismiss()
            } catch (e: Exception) {
            }
        }


    }

    private fun initFonts() {

        FontsUtils.getLatoBold(textview_title, context)
        FontsUtils.getLatoRegular(textview_body, context)
        FontsUtils.getLatoSemibold(button_yes, context)
        FontsUtils.getLatoSemibold(button_no, context)

    }


    interface OnAcceptClickListener {
        fun onAcceptClickListener()
    }

    fun showProgress(show:Boolean){
        if (show){
            rel_buttons.visibility = View.GONE
            progressbar.visibility = View.VISIBLE
        }else{
            rel_buttons.visibility = View.VISIBLE
            progressbar.visibility = View.GONE
        }
    }

}
