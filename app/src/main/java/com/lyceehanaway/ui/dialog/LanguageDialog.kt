package com.lyceehanaway.ui.dialog

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.core.content.ContextCompat
import com.lyceehanaway.R
import com.lyceehanaway.utils.FontsUtils
import com.lyceehanaway.utils.PrefUtils
import com.lyceehanaway.utils.ScreenUtils
import kotlinx.android.synthetic.main.language_dialog.*


class LanguageDialog(
    context: Context,
    private val onAcceptClickListener: OnAcceptClickListener
) : Dialog(context) {

    private var onClickListener: OnAcceptClickListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.language_dialog)
        setCancelable(false)
        this.onClickListener = onAcceptClickListener

        initFonts()



        if (PrefUtils.getLanguage(context).equals("ar")) {
            button_ar.setBackgroundResource(R.drawable.rounded_blue)
            button_ar.setTextColor(ContextCompat.getColor(context, R.color.white))
            button_en.setTextColor(ContextCompat.getColor(context, R.color.grey2))
            button_en.setBackgroundResource(R.drawable.rounded_grey)
        } else if (PrefUtils.getLanguage(context).equals("en")) {
            button_en.setBackgroundResource(R.drawable.rounded_blue)
            button_en.setTextColor(ContextCompat.getColor(context, R.color.white))
            button_ar.setTextColor(ContextCompat.getColor(context, R.color.grey2))
            button_ar.setBackgroundResource(R.drawable.rounded_grey)

        }


        button_ar.setOnClickListener {
            try {
                onAcceptClickListener.onAcceptClickListener("ar")
                dismiss()
            }catch (e:Exception){}
        }

        button_en.setOnClickListener {
            try {
                onAcceptClickListener.onAcceptClickListener("en")
                dismiss()
            }catch (e:Exception){}
        }

        window?.setLayout(
            (ScreenUtils.getScreenWidthInPixels(context) * 85) / 100,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        this.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        iamgeview_close.setOnClickListener {
            try {
                dismiss()
            } catch (e: Exception) {
            }
        }

    }

    private fun initFonts() {

        FontsUtils.getLatoBold(textview_title, context)
        FontsUtils.getLatoBold(button_ar, context)
        FontsUtils.getLatoBold(button_en, context)


    }


    interface OnAcceptClickListener {
        fun onAcceptClickListener(lang: String)
    }



}
