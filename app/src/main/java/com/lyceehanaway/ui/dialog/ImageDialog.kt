package com.lyceehanaway.ui.dialog

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import com.bumptech.glide.Glide
import com.lyceehanaway.R
import com.lyceehanaway.utils.ScreenUtils
import kotlinx.android.synthetic.main.image_dialog.*


class ImageDialog(
    context: Context,
    private val onAcceptClickListener: OnAcceptClickListener,
    var image: String
) : Dialog(context) {

    private var onClickListener: OnAcceptClickListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.image_dialog)
        setCancelable(true)
        this.onClickListener = onAcceptClickListener


        window?.setLayout(
            (ScreenUtils.getScreenWidthInPixels(context) * 90) / 100,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        this.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        try {
            Glide.with(context).load(image).into(imageview)
        } catch (e: Exception) {
        }
        imageview_close.setOnClickListener {
            try {
                dismiss()
            } catch (e: Exception) {
            }
        }
        imageview_download.setOnClickListener {
            try {
                onAcceptClickListener.onAcceptClickListener(image)
            } catch (e: Exception) {
            }
        }


    }


    interface OnAcceptClickListener {
        fun onAcceptClickListener(image: String)
    }


}
