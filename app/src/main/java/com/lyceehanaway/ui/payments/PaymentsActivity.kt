package com.lyceehanaway.ui.payments

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import bwabt.watan.utils.LocaleManager
import com.lyceehanaway.R
import com.lyceehanaway.adapter.PaymentsListAdapter
import com.lyceehanaway.model.Payment
import com.lyceehanaway.model.PaymentResp
import com.lyceehanaway.utils.FontsUtils
import com.lyceehanaway.utils.PrefUtils
import com.lyceehanaway.utils.Status
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_payments.*

@AndroidEntryPoint
class PaymentsActivity : AppCompatActivity() {

    private val viewModel: PaymentsViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payments)
        supportActionBar?.hide()

        initFonts()
        initViews()
    }

    private fun initViews() {

        imageview_back.setOnClickListener { finish() }

        if (PrefUtils.getLanguage(this).equals("ar"))
          imageview_back.rotation = 180f

        getPayments()
    }


    private fun getPayments() {

        viewModel.getPaymments().observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    progressbar.visibility = View.GONE

                    showPayments(it.data!!.data!!)


                }
                Status.LOADING -> {
                    progressbar.visibility = View.VISIBLE
                }
                Status.ERROR -> {
                    //Handle Error
                    progressbar.visibility = View.GONE
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
            }
        })

    }


    private fun showPayments(list: ArrayList<PaymentResp>) {

        recyclerview_payments.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        var adapter =
            PaymentsListAdapter(list, object : PaymentsListAdapter.ItemClickListener {
                override fun onItemClick(item: Payment) {

                }
            }
            )

        recyclerview_payments.adapter = adapter

    }

    private fun initFonts() {

        FontsUtils.getLatoBlack(textview_title, this)
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(base!!))
    }

}