package com.lyceehanaway.ui.changePassword

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import bwabt.watan.utils.LocaleManager
import com.lyceehanaway.R
import com.lyceehanaway.utils.FontsUtils
import com.lyceehanaway.utils.PrefUtils
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_change_password.*

@AndroidEntryPoint
class ChangePasswordActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)
        supportActionBar?.hide()
        initFonts()
        initViews()

    }

    private fun initViews() {

        if (PrefUtils.getLanguage(this).equals("ar"))
            imageview_back.rotation = 180f

        imageview_back.setOnClickListener { finish() }
    }

    private fun initFonts() {

        FontsUtils.getLatoBlack(textview_title, this)
        FontsUtils.getLatoRegular(edittext_curr_password, this)
        FontsUtils.getLatoRegular(edittext_conf_password, this)
        FontsUtils.getLatoRegular(edittext_new_password, this)
        FontsUtils.getLatoRegular(button_update, this)
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(base!!))
    }

}