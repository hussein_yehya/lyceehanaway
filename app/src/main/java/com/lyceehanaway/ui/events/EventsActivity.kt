package com.lyceehanaway.ui.events

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import bwabt.watan.utils.LocaleManager
import com.lyceehanaway.R
import com.lyceehanaway.adapter.SchoolEventsPaginationAdapter
import com.lyceehanaway.model.SchoolEvent
import com.lyceehanaway.model.SchoolEventResp
import com.lyceehanaway.ui.eventDetails.EventDetailsActivity
import com.lyceehanaway.utils.FontsUtils
import com.lyceehanaway.utils.PaginationScrollListener
import com.lyceehanaway.utils.PrefUtils
import com.lyceehanaway.utils.Status
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_events.*

@AndroidEntryPoint
class EventsActivity : AppCompatActivity() {

    private val viewModel: EventsViewModel by viewModels()

    private var schoolEventsPaginationAdapter: SchoolEventsPaginationAdapter? = null
    private var isLoading = false
    private var isLastPage = false
    private var currentPage = 0
    private var LIMIT = 10

    var id = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_events)
        supportActionBar?.hide()

        initViews()
        initFonts()
    }

    private fun initFonts() {

        FontsUtils.getLatoBlack(textview_title, this)
        FontsUtils.getLatoBlack(textview_empty, this)

    }

    private fun initViews() {

        try {
            id = intent.getStringExtra("id").toString()
        }catch (e:Exception){}

        if (id.equals("null")){
            textview_title.text = getString(R.string.school_events)
            id=""
        }
        else
            textview_title.text = getString(R.string.student_events)


        imageview_back.setOnClickListener { finish() }

        if (PrefUtils.getLanguage(this).equals("ar"))
            imageview_back.rotation = 180f

        getEvents()

    }


    private fun initPagingRecyclerViews() {

        currentPage = 0
        isLoading = false
        isLastPage = false

        var mLayoutManager = LinearLayoutManager(
            this,
            LinearLayoutManager.VERTICAL,
            false
        )
        schoolEventsPaginationAdapter =
            SchoolEventsPaginationAdapter(object :
                SchoolEventsPaginationAdapter.ItemClickListener {
                override fun onItemClick(item: SchoolEvent) {
                    startActivity(
                        Intent(
                            this@EventsActivity,
                            EventDetailsActivity::class.java
                        ).putExtra("event", item)
                    )


                }


            })

        recyclerview_events.layoutManager = mLayoutManager
        recyclerview_events.adapter = schoolEventsPaginationAdapter

        recyclerview_events.addOnScrollListener(object :
            PaginationScrollListener(mLayoutManager) {
            override fun loadMoreItems() {
                this@EventsActivity.isLoading = true
                this@EventsActivity.currentPage += LIMIT
                getEventsNext()

            }


            override fun isLastPage(): Boolean {
                return this@EventsActivity.isLastPage
            }

            override fun isLoading(): Boolean {
                return this@EventsActivity.isLoading
            }
        })
    }

    private fun getEventsNext() {


        viewModel.getSchoolEvents(id,LIMIT.toString(), currentPage.toString()).observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    schoolEventsPaginationAdapter!!.removeLoadingFooter()
                    isLoading = false
                    val results: List<SchoolEventResp> =
                        it.data!!.data!!
                    schoolEventsPaginationAdapter!!.addAll(results)

                    if (results.size >= LIMIT) {
                        schoolEventsPaginationAdapter!!.addLoadingFooter()

                    } else {
                        isLastPage = true

                    }


                }
                Status.LOADING -> {
                }
                Status.ERROR -> {
                    //Handle Error
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    private fun showEvents(list: ArrayList<SchoolEventResp>) {

        if (list.size==0){
            textview_empty.visibility = View.VISIBLE
        }else{
            textview_empty.visibility = View.GONE
            schoolEventsPaginationAdapter!!.clear()
            schoolEventsPaginationAdapter!!.addAll(list)


            if (list.size >= LIMIT) {
                schoolEventsPaginationAdapter!!.addLoadingFooter()

            } else {
                isLastPage = true

            }
        }


//        val mLayoutManager =
//            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
//
//
//        recyclerview_events.layoutManager = mLayoutManager
//
//        var adapter =
//            SchoolEventsListAdapter(list, object : SchoolEventsListAdapter.ItemClickListener {
//                override fun onItemClick(item: SchoolEvent) {
//
//         startActivity(Intent(this@EventsActivity, EventDetailsActivity::class.java).putExtra("event",item))
//
//                }
//            }
//            )
//
//        recyclerview_events.adapter = adapter
    }

    private fun getEvents() {

        currentPage = 0
        isLastPage = false
        isLoading = false

        initPagingRecyclerViews()

        viewModel.getSchoolEvents(id,LIMIT.toString(), currentPage.toString()).observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    progressbar.visibility = View.GONE

                    showEvents(it.data!!.data!!)


                }
                Status.LOADING -> {
                    progressbar.visibility = View.VISIBLE
                }
                Status.ERROR -> {
                    //Handle Error
                    progressbar.visibility = View.GONE
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
            }
        })

    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(base!!))
    }

}