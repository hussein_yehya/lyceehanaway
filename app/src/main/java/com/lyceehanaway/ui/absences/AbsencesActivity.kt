package com.lyceehanaway.ui.absences

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import bwabt.watan.utils.LocaleManager
import com.lyceehanaway.R
import com.lyceehanaway.adapter.AbsenceListAdapter
import com.lyceehanaway.model.Absence
import com.lyceehanaway.utils.FontsUtils
import com.lyceehanaway.utils.PrefUtils
import com.lyceehanaway.utils.Status
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_absences.*

@AndroidEntryPoint
class AbsencesActivity : AppCompatActivity() {

    private val viewModel: AbsencesViewModel by viewModels()

    var id = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_absences)
        supportActionBar?.hide()

        initFonts()
        initViews()

    }

    private fun initViews() {

        try {
            id = intent.getStringExtra("id").toString()
        } catch (e: Exception) {
        }

        getAbsence()

        if (PrefUtils.getLanguage(this).equals("ar"))
           imageview_back.rotation = 180f

        imageview_back.setOnClickListener { finish() }
    }

    private fun initFonts() {
        FontsUtils.getLatoBlack(textview_title, this)
        FontsUtils.getLatoBold(textview_total, this)
        FontsUtils.getLatoBold(textview_total_val, this)
    }

    private fun showAbsence(list: ArrayList<Absence>) {

        textview_total_val.text = list.size.toString()

        recyclerview_absence.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        var adapter =
            AbsenceListAdapter(list, object : AbsenceListAdapter.ItemClickListener {
                override fun onItemClick(item: Absence) {


                }
            }
            )

        recyclerview_absence.adapter = adapter
    }


    private fun getAbsence() {


        viewModel.getAbsence(id).observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    progressbar.visibility = View.GONE

                    showAbsence(it.data!!.data!!)


                }
                Status.LOADING -> {
                    progressbar.visibility = View.VISIBLE
                }
                Status.ERROR -> {
                    //Handle Error
                    progressbar.visibility = View.GONE
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
            }
        })

    }
    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(base!!))
    }


}