package com.lyceehanaway.ui.absences


import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.google.gson.JsonObject
import com.lyceehanaway.Application
import com.lyceehanaway.api.ApiContract
import com.lyceehanaway.repository.MainRepository
import com.lyceehanaway.utils.PrefUtils
import com.lyceehanaway.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

@HiltViewModel
class AbsencesViewModel @Inject constructor(
    private val mainRepository: MainRepository,
    private val context : android.app.Application
) : ViewModel() {
    fun getAbsence(id:String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            var jsonObject: JsonObject = JsonObject()
            jsonObject.addProperty(ApiContract.Param.id_student, id)
            emit(Resource.success(data = mainRepository.getAbsence(jsonObject)))
        } catch (exception: Exception) {
            emit(Resource.error(exception.message ?: "Error Occurred!", data = null))
        }
    }
}