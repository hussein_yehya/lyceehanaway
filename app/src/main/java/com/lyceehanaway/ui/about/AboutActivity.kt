package com.lyceehanaway.ui.about

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import bwabt.watan.utils.LocaleManager
import com.bumptech.glide.Glide
import com.lyceehanaway.R
import com.lyceehanaway.model.AboutUs
import com.lyceehanaway.ui.agenda.AgendaViewModel
import com.lyceehanaway.utils.FontsUtils
import com.lyceehanaway.utils.PrefUtils
import com.lyceehanaway.utils.Status
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_about.*

@AndroidEntryPoint
class AboutActivity : AppCompatActivity() {

    private val viewModel: AboutViewModel by viewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)
        supportActionBar?.hide()

        initFonts()

        initViews()
    }

    private fun initViews() {
        showAbout(PrefUtils.getAbout(this))
        getAbout()
        imageview_back.setOnClickListener { finish() }

        if (PrefUtils.getLanguage(this).equals("ar"))
            imageview_back.rotation = 180f

    }

    private fun initFonts() {

        FontsUtils.getLatoBlack(textview_title,this)
        FontsUtils.getLatoRegular(textview_phone,this)
        FontsUtils.getLatoRegular(textview_email,this)
        FontsUtils.getLatoRegular(textview_loaction,this)
        FontsUtils.getLatoRegular(textview_body,this)
        FontsUtils.getLatoBold(textview_title1,this)
    }

    private fun getAbout() {

        viewModel.getAbout().observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {

                    showAbout(it.data!!.data!!)

                }
                Status.LOADING -> {
                }
                Status.ERROR -> {
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
            }
        })

    }

    private fun showAbout(data: AboutUs) {

        PrefUtils.setAbout(this,data)
        textview_title1.text = data.title.toString()
        textview_phone.text = data.phone.toString()
        textview_email.text = data.email.toString()
        textview_loaction.text = data.address.toString()
        textview_body.text = data.description.toString()

        try {
            Glide.with(this).load(data.image)
                .error(R.drawable.ic_about)
                .placeholder(R.drawable.ic_about)
                .into(imageview)

        }catch (e:Exception){}
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(base!!))
    }

}