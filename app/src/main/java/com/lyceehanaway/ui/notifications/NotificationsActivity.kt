package com.lyceehanaway.ui.notifications

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import bwabt.watan.utils.LocaleManager
import com.lyceehanaway.R
import com.lyceehanaway.adapter.NotificationsAdapter
import com.lyceehanaway.adapter.NotificationsPaginationAdapter
import com.lyceehanaway.model.Notification
import com.lyceehanaway.ui.notificationDetails.NotificationDetailsActivity
import com.lyceehanaway.utils.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_notifications.*

@AndroidEntryPoint
class NotificationsActivity : AppCompatActivity(),OnReadNotification {

    companion object {
        lateinit var onReadNotification: OnReadNotification
    }

    private val viewModel: NotificationsViewModel by viewModels()

    private var notificationsPaginationAdapter: NotificationsPaginationAdapter? = null
    private var isLoading = false
    private var isLastPage = false
    private var currentPage = 0
    private var LIMIT = 20

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notifications)
        supportActionBar?.hide()

        initFonts()
        initViews()

    }


    private fun initViews() {

        onReadNotification = this

        imageview_back.setOnClickListener { finish() }

        if (PrefUtils.getLanguage(this).equals("ar"))
            imageview_back.rotation = 180f

        showCachedNotifications()


        getNotifications()

    }

    override fun onResume() {
        super.onResume()

    }

    private fun showCachedNotifications() {

                recyclerview_notifications.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        var adapter =
            NotificationsAdapter(PrefUtils.getNotifications(this), object : NotificationsAdapter.ItemClickListener {
                override fun onItemClick(item: Notification) {
                    startActivity(Intent(this@NotificationsActivity, NotificationDetailsActivity::class.java).putExtra("notification",item))


                }
            }
            )

        recyclerview_notifications.adapter = adapter
    }


    private fun initPagingRecyclerViews() {

        currentPage = 0
        isLoading = false
        isLastPage = false

        var mLayoutManager = LinearLayoutManager(
            this,
            LinearLayoutManager.VERTICAL,
            false
        )
        notificationsPaginationAdapter =
            NotificationsPaginationAdapter(object :
                NotificationsPaginationAdapter.ItemClickListener {
                override fun onItemClick(item: Notification) {
                    startActivity(
                        Intent(
                            this@NotificationsActivity,
                            NotificationDetailsActivity::class.java
                        ).putExtra("notification", item)

                    )


                }


            })

        recyclerview_notifications.layoutManager = mLayoutManager
        recyclerview_notifications.adapter = notificationsPaginationAdapter

        recyclerview_notifications.addOnScrollListener(object :
            PaginationScrollListener(mLayoutManager) {
            override fun loadMoreItems() {
                this@NotificationsActivity.isLoading = true
                this@NotificationsActivity.currentPage += LIMIT
                getNotificationsNext()

            }


            override fun isLastPage(): Boolean {
                return this@NotificationsActivity.isLastPage
            }

            override fun isLoading(): Boolean {
                return this@NotificationsActivity.isLoading
            }
        })
    }

    private fun getNotificationsNext() {
        viewModel.getNotifications(LIMIT.toString(), currentPage.toString())
            .observe(this, Observer {
                when (it.status) {
                    Status.SUCCESS -> {

                        notificationsPaginationAdapter!!.removeLoadingFooter()
                        isLoading = false
                        val results: List<Notification> =
                            it.data!!.data!!.notifications
                        notificationsPaginationAdapter!!.addAll(results)

                        if (results.size >= LIMIT) {
                            notificationsPaginationAdapter!!.addLoadingFooter()

                        } else {
                            isLastPage = true

                        }


                    }
                    Status.LOADING -> {
                    }
                    Status.ERROR -> {
                        //Handle Error
                        Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                    }
                }
            })

    }

    private fun getNotifications() {
        currentPage = 0
        isLastPage = false
        isLoading = false




        viewModel.getNotifications(LIMIT.toString(), currentPage.toString())
            .observe(this, Observer {
                when (it.status) {
                    Status.SUCCESS -> {
                        progressbar.visibility = View.GONE
                        initPagingRecyclerViews()

                        showNotifications(it.data!!.data!!.notifications)


                    }
                    Status.LOADING -> {
//                        progressbar.visibility = View.VISIBLE
                    }
                    Status.ERROR -> {
                        //Handle Error
                       progressbar.visibility = View.GONE
                        Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                    }
                }
            })

    }

    private fun showNotifications(list: ArrayList<Notification>) {

        if (list.size==0){
            textview_empty.visibility = View.VISIBLE
        }else{
            textview_empty.visibility = View.GONE

            PrefUtils.setNotifications(this,list)

            notificationsPaginationAdapter!!.clear()
            notificationsPaginationAdapter!!.addAll(list)


            if (list.size >= LIMIT) {
                notificationsPaginationAdapter!!.addLoadingFooter()
            } else {
                isLastPage = true

            }

        }




    }

    private fun initFonts() {

        FontsUtils.getLatoBlack(textview_title, this)
        FontsUtils.getLatoBlack(textview_empty, this)
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(base!!))
    }

    override fun onReadNotification(id: String) {

        try {
            notificationsPaginationAdapter!!.markAsRead(id)
        }catch (e:Exception){}
    }

}