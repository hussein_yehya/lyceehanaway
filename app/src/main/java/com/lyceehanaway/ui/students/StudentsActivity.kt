package com.lyceehanaway.ui.students

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import bwabt.watan.utils.LocaleManager
import com.lyceehanaway.R
import com.lyceehanaway.adapter.StudentsModelListAdapter
import com.lyceehanaway.model.Student
import com.lyceehanaway.ui.menu.MenuActivity
import com.lyceehanaway.utils.FontsUtils
import com.lyceehanaway.utils.PrefUtils
import com.lyceehanaway.utils.Status
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_students.*


@AndroidEntryPoint
class StudentsActivity : AppCompatActivity() {

    private val viewModel: StudentsViewModel by viewModels()

    lateinit var adapter: StudentsModelListAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_students)
        supportActionBar?.hide()

        initFonts()
        initViews()
    }

    private fun initViews() {

        imageview_back.setOnClickListener { finish() }

        if (PrefUtils.getLanguage(this).equals("ar"))
            imageview_back.rotation = 180f

        getStudents()
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(base!!))
    }

    private fun getStudents() {

        viewModel.getStudnets().observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    progressbar.visibility = View.GONE

                    showStudents(it.data!!.data!!)


                }
                Status.LOADING -> {
                    progressbar.visibility = View.VISIBLE
                }
                Status.ERROR -> {
                    //Handle Error
                    progressbar.visibility = View.GONE
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
            }
        })

    }

    private fun showStudents(list: ArrayList<Student>) {


        recyclerview_students.layoutManager = LinearLayoutManager(
            this,
            RecyclerView.VERTICAL,
            false
        )
        adapter =
            StudentsModelListAdapter(
                list,
                object : StudentsModelListAdapter.ItemClickListener {
                    override fun onItemClicked(student: Student) {
                        startActivity(
                            Intent(
                                this@StudentsActivity,
                                MenuActivity::class.java
                            ).putExtra("student", student)
                        )


                    }


                })

        recyclerview_students.adapter = adapter



        searchview.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {


                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {

                adapter.filter.filter(newText.toString())

                return false
            }
        })
    }

    private fun initFonts() {

        FontsUtils.getLatoBlack(textview_title, this)
    }
}