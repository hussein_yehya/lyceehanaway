package com.lyceehanaway.ui.notificationDetails

import android.Manifest
import android.app.DownloadManager
import android.content.Context
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.text.method.LinkMovementMethod
import android.text.util.Linkify
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import bwabt.watan.utils.LocaleManager
import com.lyceehanaway.R
import com.lyceehanaway.model.Notification
import com.lyceehanaway.ui.dialog.ImageDialog
import com.lyceehanaway.ui.notifications.NotificationsActivity
import com.lyceehanaway.utils.FontsUtils
import com.lyceehanaway.utils.PrefUtils
import com.lyceehanaway.utils.Status
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_notification_details.*
import java.io.File
import java.util.*

@AndroidEntryPoint
class NotificationDetailsActivity : AppCompatActivity() {

    private val viewModel: NotificationDetailsViewModel by viewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification_details)
        supportActionBar?.hide()
        initFonts()
        initViews()

    }

    private fun initViews() {

        try {
            var notification = intent.getParcelableExtra<Notification>("notification")!!

            showNotification(notification)
            getNotification(notification.id.toString(),notification.type.toString())


        } catch (e: Exception) {
            progressbar.visibility = View.VISIBLE

            var id = intent.getStringExtra("id").toString()
            var type = intent.getStringExtra("type").toString()
            getNotification(id,type)
        }

        imageview_back.setOnClickListener { finish() }

        if (PrefUtils.getLanguage(this).equals("ar"))
            imageview_back.rotation = 180f

    }

    private fun showNotification(notification: Notification) {

        button_view_image.setOnClickListener { showImageDialog(notification.photo_url.toString()) }

        textview_title1.text = notification.title
        textview_date.text = notification.date
        textview_body.text = notification.description

        textview_body.movementMethod = LinkMovementMethod.getInstance()
        Linkify.addLinks(textview_body, Linkify.WEB_URLS or Linkify.PHONE_NUMBERS)


        if (notification.photo_url.toString().equals("")) {
            button_view_image.visibility = View.GONE
        } else {
            button_view_image.visibility = View.VISIBLE
        }
    }

    private fun initFonts() {

        FontsUtils.getLatoBlack(textview_title, this)
        FontsUtils.getLatoBold(textview_title1, this)
        FontsUtils.getLatoRegular(textview_body, this)
        FontsUtils.getLatoRegular(textview_date, this)
        FontsUtils.getLatoRegular(button_view_image, this)
    }

    private fun showImageDialog(item: String) {

        var dialog = ImageDialog(this, object : ImageDialog.OnAcceptClickListener {
            override fun onAcceptClickListener(image: String) {


                if (checkPermission()) {
                    downloadImageNew(
                        "${Calendar.getInstance().timeInMillis}.jpg", image
                    )
                }
            }

        }, item)

        try {
            dialog.show()
        } catch (e: java.lang.Exception) {
        }
    }


    fun checkPermission(): Boolean {
        val READ_EXTERNAL_PERMISSION =
            ContextCompat.checkSelfPermission(
                this@NotificationDetailsActivity,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
        if (READ_EXTERNAL_PERMISSION != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                this@NotificationDetailsActivity,
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                101
            )
            return false
        }
        return true
    }


    private fun downloadImageNew(filename: String, downloadUrlOfImage: String) {
        try {
            val dm = getSystemService(DOWNLOAD_SERVICE) as DownloadManager
            val downloadUri = Uri.parse(downloadUrlOfImage)
            val request = DownloadManager.Request(downloadUri)
            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI or DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false)
                .setTitle(filename)
                .setMimeType("image/jpeg") // Your file type. You can use this code to download other file types also.
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                .setDestinationInExternalPublicDir(
                    Environment.DIRECTORY_PICTURES,
                    File.separator + filename + ".jpg"
                )
            dm.enqueue(request)
            Toast.makeText(this, "Image download started.", Toast.LENGTH_SHORT).show()
        } catch (e: java.lang.Exception) {
            Toast.makeText(this, "Image download failed.", Toast.LENGTH_SHORT).show()
        }
    }


    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(base!!))
    }


    private fun getNotification(id: String,type: String) {

        viewModel.getNotification(id,type)
            .observe(this, Observer {
                when (it.status) {
                    Status.SUCCESS -> {
                        progressbar.visibility = View.GONE

                        try {
                            NotificationsActivity.onReadNotification.onReadNotification(id)
                        } catch (e: Exception) {
                        }
                        try {
                            showNotification(it.data!!.data!!.notifications[0])
                        } catch (e: Exception) {
                        }

                    }
                    Status.LOADING -> {
                    }
                    Status.ERROR -> {
                        progressbar.visibility = View.GONE
                        //Handle Error
                        Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                    }
                }
            })

    }


}