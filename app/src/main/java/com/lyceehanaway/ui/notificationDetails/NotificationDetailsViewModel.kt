package com.lyceehanaway.ui.notificationDetails


import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.google.gson.JsonObject
import com.lyceehanaway.Application
import com.lyceehanaway.api.ApiContract
import com.lyceehanaway.repository.MainRepository
import com.lyceehanaway.utils.PrefUtils
import com.lyceehanaway.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

@HiltViewModel
class NotificationDetailsViewModel @Inject constructor(
    private val mainRepository: MainRepository,
    private val context : android.app.Application
) : ViewModel() {
    fun getNotification(id:String,type: String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))

        var jsonObject: JsonObject = JsonObject()
        jsonObject.addProperty(ApiContract.Param.id, id)
        jsonObject.addProperty(ApiContract.Param.type, type)

        try {
            emit(Resource.success(data = mainRepository.getNotifications(jsonObject)))
        } catch (exception: Exception) {
            emit(Resource.error(exception.message ?: "Error Occurred!", data = null))
        }
    }
}