package com.lyceehanaway.ui.splash

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.messaging.FirebaseMessaging
import com.lyceehanaway.R
import com.lyceehanaway.ui.home.HomeActivity
import com.lyceehanaway.ui.login.LoginActivity
import com.lyceehanaway.utils.PrefUtils
import android.provider.Settings.Secure
import com.lyceehanaway.ui.eventDetails.EventDetailsActivity
import com.lyceehanaway.ui.events.EventsActivity
import com.lyceehanaway.ui.grades.GradesActivity
import com.lyceehanaway.ui.notificationDetails.NotificationDetailsActivity
import com.lyceehanaway.ui.notifications.NotificationsActivity

class SplashActivity : AppCompatActivity() {

    var title = ""
    var body = ""
    var type = ""
    var idstudent = ""
    var idcard = ""
    var idcard_element = ""
    var id = ""
    var notification_type = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        supportActionBar?.hide()

        initViews()
    }

    private fun initViews() {

        title = intent.extras?.getString("title").toString()
        body = intent.extras?.getString("body").toString()
        type = intent.extras?.getString("type").toString()
        idstudent = intent.extras?.getString("idstudent").toString()
        idcard = intent.extras?.getString("idcard").toString()
        idcard_element = intent.extras?.getString("idcard_element").toString()
        id = intent.extras?.getString("id").toString()
        notification_type = intent.extras?.getString("notification_type").toString()

        getInit()
        getNotifToken()

        Handler().postDelayed({

            if (PrefUtils.getLoggedIn(this)) {
                if (!type.equals("null")){
                    when(type){

                        "1"->{
                            startActivity(
                                Intent(
                                    this,
                                    NotificationDetailsActivity::class.java
                                ).putExtra("id",id).putExtra("type",notification_type)
                            )
                        }
                        "2"->{
                            startActivity(
                                Intent(
                                    this,
                                    EventDetailsActivity::class.java
                                ).putExtra("id",id)
                            )
                        }
                        "3"->{
                            startActivity(
                                Intent(
                                    this,
                                    GradesActivity::class.java
                                ).putExtra("idstudent",idstudent)
                                .putExtra("idcard",idcard)
                                .putExtra("idcard_element",idcard_element)
                            )

                        }else->{
                        startActivity(Intent(this@SplashActivity, HomeActivity::class.java))

                    }
                    }

                }else{
                    startActivity(Intent(this@SplashActivity, HomeActivity::class.java))

                }

            } else {
                startActivity(Intent(this@SplashActivity, LoginActivity::class.java))
            }
            finish()

        }, 2000)
    }



    private fun getNotifToken() {
        try {

            FirebaseMessaging.getInstance().token.addOnCompleteListener {
                // Get new Instance ID token

                if (it.isComplete) {
                    try {
                        val token = it.result.toString()
                        if (token != null)
                            PrefUtils.setNotificationToken(application, token)
                    } catch (e: Exception) {
                    }

                }


            }.addOnFailureListener {

            }

        } catch (e: Exception) {
        }


    }


    fun getInit() {


        try {
            val release = Build.VERSION.RELEASE
            Log.d("info_app_sdk", release.toString())
            PrefUtils.setOsVer(application, release)

        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        try {

            PrefUtils.setDevName(application, getDeviceName())

        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        try {
            var android_id = Secure.getString(getContentResolver(),
                Secure.ANDROID_ID);
            PrefUtils.setDeviceId(application, android_id)

        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }


        try {
            val pInfo = application.packageManager.getPackageInfo(application.packageName, 0)
            val version = pInfo.versionName
            val versionCode = pInfo.versionCode
            PrefUtils.setAppVer(application, version)
            PrefUtils.setAppBuild(application, versionCode.toString())
            Log.d("info_app_ver", version.toString())

        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }


    }


    fun getDeviceName(): String {
        val manufacturer = Build.MANUFACTURER
        val model = Build.MODEL
        return if (model.startsWith(manufacturer)) {
            capitalize(model)
        } else {
            capitalize(manufacturer) + " " + model
        }
    }


    private fun capitalize(s: String?): String {
        if (s == null || s.length == 0) {
            return ""
        }
        val first = s[0]
        return if (Character.isUpperCase(first)) {
            s
        } else {
            Character.toUpperCase(first) + s.substring(1)
        }
    }


}