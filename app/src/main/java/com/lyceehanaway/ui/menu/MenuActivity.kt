package com.lyceehanaway.ui.menu

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import bwabt.watan.utils.LocaleManager
import com.lyceehanaway.R
import com.lyceehanaway.model.Menu
import com.lyceehanaway.adapter.StudentsMenuListAdapter
import com.lyceehanaway.model.Student
import com.lyceehanaway.ui.absences.AbsencesActivity
import com.lyceehanaway.ui.agenda.AgendaActivity
import com.lyceehanaway.ui.events.EventsActivity
import com.lyceehanaway.ui.grades.GradesActivity
import com.lyceehanaway.ui.parentMeetings.ParentMeetingsActivity
import com.lyceehanaway.ui.schedual.SchedualActivity
import com.lyceehanaway.ui.studentEvents.StudentEventsActivity
import com.lyceehanaway.utils.FontsUtils
import com.lyceehanaway.utils.PrefUtils
import kotlinx.android.synthetic.main.activity_menu.*

class MenuActivity : AppCompatActivity() {

    lateinit var student:Student

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
        supportActionBar?.hide()

        initFonts()
        initViews()
    }

    private fun initViews() {

        try {
            student = intent.getParcelableExtra<Student>("student")!!
            textview_title.text = student.fullname.toString()
        }catch (e:Exception){}

        imageview_back.setOnClickListener { finish() }

        if (PrefUtils.getLanguage(this).equals("ar"))
            imageview_back.rotation = 180f

        showMenu()
    }

    private fun showMenu() {

        var list = ArrayList<Menu>()

        list.add(Menu(1,R.drawable.book, getString(R.string.Agenda)))
        list.add(Menu(2,R.drawable.grade, getString(R.string.Grades)))
        list.add(Menu(3,R.drawable.absence, getString(R.string.Absences)))
        list.add(Menu(4,R.drawable.student_events, getString(R.string.student_events)))
        list.add(Menu(5,R.drawable.schedual, getString(R.string.class_schedule)))
        list.add(Menu(6,R.drawable.parent_meeting, getString(R.string.parent_meetings)))



        try {

            val mLayoutManager =
                LinearLayoutManager(this,   LinearLayoutManager.VERTICAL, false)


            recyclerview_menu.layoutManager = mLayoutManager

            var adapter =
                StudentsMenuListAdapter(list, object : StudentsMenuListAdapter.ItemClickListener {
                    override fun onItemClick(item: Menu) {


                        when(item.id){


                            1->{
                                startActivity(Intent(this@MenuActivity, AgendaActivity::class.java).putExtra("id",student.classSectionID.toString()))

                            }
                            2->{
                                startActivity(Intent(this@MenuActivity, GradesActivity::class.java).putExtra("id",student.id.toString()))

                            }
                            3->{
                                startActivity(Intent(this@MenuActivity, AbsencesActivity::class.java).putExtra("id",student.id.toString()))

                            }
                            4->{
                              startActivity(Intent(this@MenuActivity, EventsActivity::class.java).putExtra("id",student.id.toString()))

                            }
                            5->{
                               startActivity(Intent(this@MenuActivity, SchedualActivity::class.java).putExtra("id",student.id.toString()).putExtra("section_id",student.classSectionID.toString()))

                            }
                            6->{
                                startActivity(Intent(this@MenuActivity, ParentMeetingsActivity::class.java).putExtra("id",student.id.toString()).putExtra("section_id",student.classSectionID.toString()))

                            }
                        }

                    }


                })
            recyclerview_menu.adapter = adapter
        } catch (e: Exception) {
        }

    }

    private fun initFonts() {
        FontsUtils.getLatoBlack(textview_title,this)
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(base!!))
    }

}