package com.lyceehanaway.ui.grades


import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.google.gson.JsonObject
import com.lyceehanaway.api.ApiContract
import com.lyceehanaway.repository.MainRepository
import com.lyceehanaway.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

@HiltViewModel
class GradesViewModel @Inject constructor(
        private val mainRepository: MainRepository,
        private val context: android.app.Application
) : ViewModel() {

    fun getGrades(idstudent: String, idcard: String, idcard_element: String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            var jsonObject: JsonObject = JsonObject()
            jsonObject.addProperty(ApiContract.Param.idstudent, idstudent)
            jsonObject.addProperty(ApiContract.Param.idcard, idcard)
            jsonObject.addProperty(ApiContract.Param.idcard_element, idcard_element)

            Log.d("resss__", jsonObject.toString())

            emit(Resource.success(data = mainRepository.getGrades(jsonObject)))
        } catch (exception: Exception) {
            emit(Resource.error(exception.message ?: "Error Occurred!", data = null))
        }
    }

    fun getCards(idstudent: String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            var jsonObject: JsonObject = JsonObject()
            jsonObject.addProperty(ApiContract.Param.idstudent, idstudent)
            jsonObject.addProperty(ApiContract.Param.idyear, "2021")

            Log.d("resss__", jsonObject.toString())

            emit(Resource.success(data = mainRepository.getCards(jsonObject)))
        } catch (exception: Exception) {
            emit(Resource.error(exception.message ?: "Error Occurred!", data = null))
        }
    }
}