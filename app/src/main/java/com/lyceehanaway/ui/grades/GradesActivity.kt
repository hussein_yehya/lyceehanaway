package com.lyceehanaway.ui.grades

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import bwabt.watan.utils.LocaleManager
import com.lyceehanaway.R
import com.lyceehanaway.adapter.GradesListAdapter
import com.lyceehanaway.model.Card
import com.lyceehanaway.model.CardElement
import com.lyceehanaway.model.GradesResp
import com.lyceehanaway.ui.dialog.CardElementsDialog
import com.lyceehanaway.ui.dialog.CardsDialog
import com.lyceehanaway.utils.FontsUtils
import com.lyceehanaway.utils.PrefUtils
import com.lyceehanaway.utils.Status
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_grades.*

@AndroidEntryPoint
class GradesActivity : AppCompatActivity() {

    private val viewModel: GradesViewModel by viewModels()
    var cardsList = ArrayList<Card>()

    var selectedCard:Card? = null
    var selectedElement:CardElement? = null
    var id= ""
    var idcard_element= ""
    var idcard= ""
    var idstudent= ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_grades)
        supportActionBar?.hide()
        initFonts()
        initViews()

    }

    private fun initViews() {

        imageview_back.setOnClickListener { finish() }

        if (PrefUtils.getLanguage(this).equals("ar"))
            imageview_back.rotation = 180f

         try {
             id = intent.getStringExtra("id").toString()

             if (id.equals("")|| id.equals("null")){
                 idcard_element = intent.getStringExtra("idcard_element").toString()
                 idcard = intent.getStringExtra("idcard").toString()
                 idstudent = intent.getStringExtra("idstudent").toString()
                 getGrades(idstudent,idcard,idcard_element)
             }else{
                 getCards()

             }
         }catch (e:Exception){
             idcard_element = intent.getStringExtra("idcard_element").toString()
             idcard = intent.getStringExtra("idcard").toString()
             idstudent = intent.getStringExtra("idstudent").toString()
             getGrades(idstudent,idcard,idcard_element)

         }

     }

    private fun initFonts() {
        FontsUtils.getLatoBlack(textview_title, this)
        FontsUtils.getLatoBold(textview_select_card, this)
        FontsUtils.getLatoBold(textview_select_element, this)
    }

    private fun showSchedual(list: ArrayList<GradesResp>) {

        recyclerview_grades.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        var adapter =
            GradesListAdapter(list, object : GradesListAdapter.ItemClickListener {
                override fun onItemClick(item: GradesResp) {


                }

            }
            )

        recyclerview_grades.adapter = adapter
    }

    private fun getGrades(idStudent:String,idCard:String,idcardElements:String,) {
        viewModel.getGrades(idStudent,idCard,idcardElements).observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    progressbar.visibility = View.GONE

                    showSchedual(it.data!!.data!!)


                }
                Status.LOADING -> {
                    progressbar.visibility = View.VISIBLE
                }
                Status.ERROR -> {
                    //Handle Error
                    progressbar.visibility = View.GONE
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
            }
        })

    }

    private fun getCards() {
        viewModel.getCards(id).observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    progressbar.visibility = View.GONE

                    showCards(it.data!!.data!!)


                }
                Status.LOADING -> {
                    progressbar.visibility = View.VISIBLE
                }
                Status.ERROR -> {
                    //Handle Error
                    progressbar.visibility = View.GONE
                    rel_sel_cards.visibility = View.GONE
                    rel_sel_elements.visibility = View.GONE
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
            }
        })

    }

    private fun showCards(data: java.util.ArrayList<Card>) {
        cardsList = data
        rel_sel_cards.visibility = View.VISIBLE
        rel_sel_elements.visibility = View.GONE

        rel_sel_cards.setOnClickListener {
            showCardsDialog()
        }
        imageview_search.setOnClickListener {
            showCardsDialog()
        }
        textview_select_card.setOnClickListener {
            showCardsDialog()
        }
        imageview_arrow_down.setOnClickListener {
            showCardsDialog()
        }

    }

    private fun showCardsDialog() {
        Log.d("Adadadd__","showCardsDialog")

        var dialog = CardsDialog(this, cardsList, object : CardsDialog.OnAcceptClickListener {
            override fun onAcceptClickListener(card: Card) {
                selectedCard = card
                textview_select_card.text = selectedCard!!.cardTitle.toString()
                rel_sel_elements.visibility = View.VISIBLE
                selectedElement = null
                textview_select_element.text = getString(R.string.select_element)

                showElementsDialog()

                rel_sel_elements.setOnClickListener { showElementsDialog() }

            }


        })

        try {
            dialog.show()
        } catch (e: Exception) {
            Log.d("Adadadd__",e.message.toString())
        }
    }

    private fun showElementsDialog() {

        var dialog = CardElementsDialog(this, selectedCard!!, object : CardElementsDialog.OnAcceptClickListener {
            override fun onAcceptClickListener(cardElement: CardElement) {

                    selectedElement = cardElement
                    textview_select_element.text = selectedElement!!.display.toString()

                getGrades(id, selectedCard!!.idclassCard.toString(), selectedElement!!.cardElements_idcardElements.toString())

            }


        })

        try {
            dialog.show()
        } catch (e: Exception) {
        }
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(base!!))
    }

}