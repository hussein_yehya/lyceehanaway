package com.lyceehanaway.ui.annualCalender

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import bwabt.watan.utils.LocaleManager
import com.lyceehanaway.R
import com.lyceehanaway.adapter.AnnualCalenderListAdapter
import com.lyceehanaway.model.Calender
import com.lyceehanaway.ui.agenda.AgendaViewModel
import com.lyceehanaway.ui.calender.CalenderActivity
import com.lyceehanaway.utils.FontsUtils
import com.lyceehanaway.utils.PrefUtils
import com.lyceehanaway.utils.Status
import com.prolificinteractive.materialcalendarview.CalendarDay
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_annual_calender.*

@AndroidEntryPoint
class AnnualCalenderActivity : AppCompatActivity() {

    private val viewModel: AnnualCalenderViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_annual_calender)
        supportActionBar?.hide()

        initFonts()
        initViews()

    }

    private fun initViews() {

        imageview_back.setOnClickListener { finish() }

        button_annual_calender.setOnClickListener {
            startActivity(Intent(this@AnnualCalenderActivity, CalenderActivity::class.java))
        }

        if (PrefUtils.getLanguage(this).equals("ar"))
             imageview_back.rotation = 180f

        getAnnualCalender()
    }

    private fun showEvents(list:ArrayList<Calender>) {


        recyclerview_annual_calender.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        var adapter =
            AnnualCalenderListAdapter(list, object : AnnualCalenderListAdapter.ItemClickListener {
                override fun onItemClick(item: Calender) {


                }
            }
            )

        recyclerview_annual_calender.adapter = adapter
    }


    private fun initFonts() {

        FontsUtils.getLatoBlack(textview_title, this)
        FontsUtils.getLatoRegular(button_annual_calender, this)

    }

    private fun getAnnualCalender() {


        viewModel.getAnnualCalender().observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    progressbar.visibility = View.GONE

                    showEvents(it.data!!.data!!.calendar)
//                    showDates(it.data!!.data!!.calendarDates)


                }
                Status.LOADING -> {
                    progressbar.visibility = View.VISIBLE
                }
                Status.ERROR -> {
                    //Handle Error
                    progressbar.visibility = View.GONE
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
            }
        })

    }


    private fun showDates(calendarDates: ArrayList<String>) {

        var dates = ArrayList<CalendarDay>()

        for (date in calendarDates){

            Log.d("date__",date)
            var y = date.substringBefore("-")
            var d = date.substringAfterLast("-")
            var m = date.substringBeforeLast("-").substringAfterLast("-")
            Log.d("date__y",y)
            Log.d("date__d",d)
            Log.d("date__m",m)

        }

//        calendarView.addDecorators(BeforeDecorator(R.color.grey2,dates))
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(base!!))
    }

}