package com.lyceehanaway.ui.annualCalender


import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.google.gson.JsonObject
import com.lyceehanaway.Application
import com.lyceehanaway.api.ApiContract
import com.lyceehanaway.repository.MainRepository
import com.lyceehanaway.utils.PrefUtils
import com.lyceehanaway.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

@HiltViewModel
class AnnualCalenderViewModel @Inject constructor(
    private val mainRepository: MainRepository,
    private val context : android.app.Application
) : ViewModel() {
    fun getAnnualCalender() = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            var jsonObject: JsonObject = JsonObject()
            jsonObject.addProperty(ApiContract.Param.idparent, PrefUtils.getUserId(context))


            emit(Resource.success(data = mainRepository.getAnnualCalender()))
        } catch (exception: Exception) {
            emit(Resource.error(exception.message ?: "Error Occurred!", data = null))
        }
    }
}