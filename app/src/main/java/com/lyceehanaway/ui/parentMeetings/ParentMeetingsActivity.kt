package com.lyceehanaway.ui.parentMeetings

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.format.DateFormat
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import bwabt.watan.utils.LocaleManager
import com.lyceehanaway.R
import com.lyceehanaway.adapter.SchedualListAdapter
import com.lyceehanaway.model.Schedual
import com.lyceehanaway.model.SchedualListResp
import com.lyceehanaway.utils.FontsUtils
import com.lyceehanaway.utils.PrefUtils
import com.lyceehanaway.utils.Status
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_parent_meetings.*
import java.util.*
import kotlin.collections.ArrayList

@AndroidEntryPoint
class ParentMeetingsActivity : AppCompatActivity() {

    private val viewModel: ParentMeetingsViewModel by viewModels()

    var id = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_parent_meetings)
        supportActionBar?.hide()

        initFonts()
        initViews()
    }

    private fun initViews() {

        try {
            id = intent.getStringExtra("id").toString()
        }catch (e:Exception){}

        imageview_back.setOnClickListener { finish() }

        if (PrefUtils.getLanguage(this).equals("ar"))
            imageview_back.rotation = 180f

        getParentMeetings()
    }

    private fun initFonts() {
        FontsUtils.getLatoBlack(textview_mon, this)
        FontsUtils.getLatoBlack(textview_thu, this)
        FontsUtils.getLatoBlack(textview_tue, this)
        FontsUtils.getLatoBlack(textview_wed, this)
        FontsUtils.getLatoBlack(textview_fri, this)
        FontsUtils.getLatoBlack(textview_title, this)
    }





    private fun showSchedual(list: ArrayList<Schedual>) {
        lin_week.visibility = View.VISIBLE
        recyclerview_schedual.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        var adapter =
            SchedualListAdapter(list, object : SchedualListAdapter.ItemClickListener {
                override fun onItemClick(item: Schedual) {


                }
            }
            )

        recyclerview_schedual.adapter = adapter
    }

    private fun getParentMeetings() {
        viewModel.getParentMeetings(id ).observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    progressbar.visibility = View.GONE

                    shwoSchedualByDay(it.data!!.data!!)


                }
                Status.LOADING -> {
                    progressbar.visibility = View.VISIBLE
                }
                Status.ERROR -> {
                    //Handle Error
                    progressbar.visibility = View.GONE
                    lin_week.visibility = View.GONE
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
            }
        })

    }

    private fun shwoSchedualByDay(schedualListResp: SchedualListResp) {

        textview_mon.setOnClickListener {
            showSchedual(schedualListResp.mon)
            textview_mon.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white))
            textview_tue.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.off_white))
            textview_wed.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.off_white))
            textview_thu.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.off_white))
            textview_fri.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.off_white))

        }

        textview_tue.setOnClickListener {
            showSchedual(schedualListResp.tue)

            textview_mon.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.off_white))
            textview_tue.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white))
            textview_wed.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.off_white))
            textview_thu.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.off_white))
            textview_fri.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.off_white))

        }

        textview_wed.setOnClickListener {
            showSchedual(schedualListResp.wed)


            textview_mon.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.off_white))
            textview_tue.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.off_white))
            textview_wed.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white))
            textview_thu.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.off_white))
            textview_fri.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.off_white))
        }

        textview_thu.setOnClickListener {
            showSchedual(schedualListResp.thu)

            textview_mon.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.off_white))
            textview_tue.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.off_white))
            textview_wed.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.off_white))
            textview_thu.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white))
            textview_fri.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.off_white))
        }

        textview_fri.setOnClickListener {
            showSchedual(schedualListResp.fri)


            textview_mon.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.off_white))
            textview_tue.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.off_white))
            textview_wed.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.off_white))
            textview_thu.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.off_white))
            textview_fri.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white))
        }

        val date = Calendar.getInstance()
        val dayToday = DateFormat.format("EEEE", date).toString()

        Log.d("dayToday",dayToday)

        when(dayToday.toLowerCase()){
            "monday"->{
                textview_mon.callOnClick()

            }
            "tuesday"->{
                textview_tue.callOnClick()

            }
            "wednesday"->{
                textview_wed.callOnClick()

            }
            "thursday"->{
                textview_thu.callOnClick()

            }
            "friday"->{
                textview_fri.callOnClick()

            }else->{
            textview_mon.callOnClick()
        }
        }

    }


    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(base!!))
    }

}