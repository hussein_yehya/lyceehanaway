package com.lyceehanaway.ui.calender

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import bwabt.watan.utils.LocaleManager
import com.lyceehanaway.R
import com.lyceehanaway.adapter.AnnualCalenderListAdapter
import com.lyceehanaway.model.Calender
import com.lyceehanaway.ui.annualCalender.AnnualCalenderActivity
import com.lyceehanaway.utils.*
import com.prolificinteractive.materialcalendarview.CalendarDay
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_calender.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


@AndroidEntryPoint
class CalenderActivity : AppCompatActivity() {

    private val viewModel: CalenderViewModel by viewModels()
    var currDateLoaded = false

    var date = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calender)
        supportActionBar?.hide()
        initFonts()
        initViews()

    }

    private fun initViews() {

        imageview_back.setOnClickListener { finish() }

        if (PrefUtils.getLanguage(this).equals("ar"))
            imageview_back.rotation = 180f


        calendarView.setOnDateChangedListener { widget, date, selected ->

            var month: String = date.month.toString()
            if (month.toString().length == 1)
                month = "0$month"

            var day: String = date.day.toString()
            if (day.toString().length == 1)
                day = "0$day"

            this.date = "${date.year}-${month}-${day}"
            getCalenderByDate(this.date)
        }

//        val c = Calendar.getInstance().time
//        val df = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
//        val formattedDate: String = df.format(c)
//        date = formattedDate
//
//
//        calendarView.setSelectedDate(CalendarDay.today())


        getCalenderByDate("")
    }


    private fun initFonts() {

        FontsUtils.getLatoBlack(textview_title, this)
    }

    private fun showEvents(list: ArrayList<Calender>) {

        recyclerview_events.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        var adapter =
            AnnualCalenderListAdapter(list, object : AnnualCalenderListAdapter.ItemClickListener {
                override fun onItemClick(item: Calender) {


                }
            }
            )

        recyclerview_events.adapter = adapter
    }

    private fun getCalenderByDate(d:String) {

        viewModel.getCalenderByDate(d).observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    progressbar.visibility = View.GONE

                    if (!currDateLoaded){
                        val c = Calendar.getInstance().time
                        val df = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                        val formattedDate: String = df.format(c)
                        date = formattedDate

                        currDateLoaded = true

                        showDates(it.data!!.data!!.calendarDates)

                        calendarView.setSelectedDate(CalendarDay.today())

                    }else{
                        showEvents(it.data!!.data!!.calendar)

                    }


                }
                Status.LOADING -> {
                    progressbar.visibility = View.VISIBLE
                }
                Status.ERROR -> {
                    //Handle Error
                    progressbar.visibility = View.GONE
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
            }
        })

    }

    private fun showDates(calendarDates: ArrayList<String>) {

        var datesBef = ArrayList<CalendarDay>()
        var datesAft = ArrayList<CalendarDay>()
        var datesNow = ArrayList<CalendarDay>()

        for (date in calendarDates){

            Log.d("date__",date)
            var y = date.substringBefore("-")
            var d = date.substringAfterLast("-")
            var m = date.substringBeforeLast("-").substringAfterLast("-")
            Log.d("date__y",y)
            Log.d("date__d",d)
            Log.d("date__m",m)

            var cd= CalendarDay.from(y.toInt(),m.toInt(),d.toInt())
            if (cd.isBefore(CalendarDay.today())){
                datesBef.add(cd)
                }

            if (cd.isAfter(CalendarDay.today())){
                datesAft.add(cd)
                }
            if (cd==CalendarDay.today()){
                datesNow.add(cd)
             }

            calendarView.setSelectedDate(cd)

        }
         calendarView.addDecorators(
             BeforeDecorator(datesBef,this),
             TodayDecorator(datesNow,this),
             AfterDecorator(datesAft,this)
         )


    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(base!!))
    }

}