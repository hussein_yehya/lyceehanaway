package com.lyceehanaway

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.AudioAttributes
import android.os.Build
import android.os.SystemClock
import android.util.Log
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.lyceehanaway.ui.splash.SplashActivity
import org.json.JSONObject


class MyFirebaseMessagingService : FirebaseMessagingService() {

    private val CHANNEL_NAME = "lyceehanaway"
    private val CHANNEL_DESC = "Firebase Cloud Messaging lyceehanaway"
    private var numMessages = 0



    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)

            sendNotification(remoteMessage)

    }


    private fun sendNotification(remoteMessage: RemoteMessage?) {

        Log.d("remote_fcm", remoteMessage!!.data.toString())
        try{
            val data = JSONObject(remoteMessage.data.toString())
            Log.d("remote_fcm",data.toString())

        }catch (e:Exception){}


        val intent: Intent = Intent(this, SplashActivity::class.java)
        var title = ""
        var body = ""
        var type = ""
        var idstudent = ""
        var idcard = ""
        var idcard_element = ""
        var id = ""
        var notification_type = ""

        val notificationBuilder =
            NotificationCompat.Builder(this, getString(R.string.notification_channel_id))

        try {

            try {
                title = remoteMessage.notification!!.title.toString()
            } catch (e: Exception) {
            }

            try {
                body = remoteMessage.notification!!.body.toString()
            } catch (e: Exception) {
            }



            if (title.equals("") || title == null)
                try {
                    title = remoteMessage.data.get("title").toString()
                } catch (e: Exception) {
                }

            if (body.equals("") || body == null)
                try {
                    body = remoteMessage.data.get("body").toString()
                } catch (e: Exception) {
                }


            val data = JSONObject(remoteMessage.data as Map<*, *>?)




            try {
                type = data.get("type").toString()
                intent.putExtra("type",type)
            } catch (e: Exception) {
            }


            try {
                idstudent = data.get("idstudent").toString()
                intent.putExtra("idstudent",idstudent)

            } catch (e: Exception) {
            }

            try {
                notification_type = data.get("notification_type").toString()
                intent.putExtra("notification_type",notification_type)
            } catch (e: Exception) {
            }

            try {
                idcard = data.get("idcard").toString()
                intent.putExtra("idcard",idcard)

            } catch (e: Exception) {
            }

            try {
                idcard_element = data.get("idcard_element").toString()
                intent.putExtra("idcard_element",idcard_element)

            } catch (e: Exception) {
            }


            try {
                id = data.get("id").toString()
                intent.putExtra("id",id)
            } catch (e: Exception) {
            }



//            try {
//                MainActivity.onReload.onReload()
//            } catch (e: Exception) {
//            }

//            Log.i("remote_fcm", order_status)
//            Log.i("remote_fcm", order_id)


            val pendingIntent =
                PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)

            notificationBuilder
                .setContentTitle(title)
                .setContentText(body)
                .setAutoCancel(true)
//                .setSound(sound)
                .setContentIntent(pendingIntent)
                .setLargeIcon(BitmapFactory.decodeResource(resources, R.drawable.logo))
                .setLights(application.resources.getColor(R.color.purple_200), 1000, 300)
                .setDefaults(Notification.DEFAULT_VIBRATE)
                .setNumber(++numMessages)
                .setSmallIcon(R.drawable.logo)


            var notificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val channel = NotificationChannel(
                    getString(R.string.notification_channel_id),
                    CHANNEL_NAME,
                    NotificationManager.IMPORTANCE_DEFAULT
                )

                val audioAttributes = AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build()

                channel.description = CHANNEL_DESC
                channel.setShowBadge(true)
                channel.canShowBadge()
                channel.enableLights(true)
//            channel.setSound(sound, audioAttributes)
                channel.lightColor = Color.RED
                channel.enableVibration(true)
                channel.vibrationPattern = longArrayOf(100, 200, 300, 400, 500)

                assert(notificationManager != null)
                notificationManager.createNotificationChannel(channel)
            }


            assert(notificationManager != null)
            notificationManager.notify(
                SystemClock.currentThreadTimeMillis().toInt(),
                notificationBuilder.build()
            )
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                Log.d(
                    "notification_count2",
                    notificationManager.activeNotifications.size.toString()
                )
            }

        } catch (e: Exception) {
            Log.d("remote_fcm", e.message.toString())
        }
    }


}


